$(document).ready(function(){

	alertify.defaults = {

        modal:true,
        movable:true,
        resizable:true,
        closable:true,
        maximizable:true,
        pinnable:true,
        pinned:true,
        padding: true,
        overflow:true,
        maintainFocus:true,
        transition:'zoom',

        // notifier defaults
        notifier:{
            // auto-dismiss wait time (in seconds)  
            delay:5,
            // default position
            position:'top-center'
        },

        // language resources 
        glossary:{
            // dialogs default title
            title:'',
            // ok button text
            ok: 'OK',
            // cancel button text
            cancel: 'Cancel'
        },

        // theme machine_settings
        theme:{
            // class name attached to prompt dialog input textbox.
            input:'ajs-input',
            // class name attached to ok button
            ok:'ajs-ok',
            // class name attached to cancel button 
            cancel:'ajs-cancel'
        }
    };

    function preload(arrayOfImages) {
        $(arrayOfImages).each(function(){
            $('<img/>')[0].src = this;
        });
    }


    preload([
        '/images/loading-bar.gif',
        '/images/coin_brass.png',
        '/images/coin_copper.png',
        '/images/coin_nickel_silver.png'

    ]);

    $('.searchable_select').chosen();

    /**
     * Only show update/save button when changes have been made
     */
    var disabled;
    $('form.edit').each(function(){

        $(this).data('serialized', $(this).serialize());

    }).on('change input', function(){

        if($(this).serialize() != $(this).data('serialized') || $(this).find('input[type="file"]').val() ){
            $(this).find('input:submit, button:submit').prop('disabled', false);
        }else{
            $(this).find('input:submit, button:submit').prop('disabled', true);
        }

    }).find('input:submit, button:submit').prop('disabled', true);


    /**
     * date picker for all inputs with datepicker class
     */
    $('.datepicker').datepicker({
        autoclose               :   true,
        format                  :   'mm/dd/yyyy',
        //endDate                 :   'day:1',
        todayBtn                :   true,
        todayHighlight          :   true,
        disableTouchKeyboard    :   true
    });


    //http://weareoutman.github.io/clockpicker/
    $('.clockpicker').clockpicker({        
        autoclose: true,
        placement: 'top',                
    });

});
