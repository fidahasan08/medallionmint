<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
		{
			$table->increments('id');
            $table->tinyInteger('active')->default(1);
			$table->integer('company_id')->unsigned();
			$table->text('name');
			$table->text('address');
			$table->text('website');
			$table->text('shipping_method');
			$table->text('corporate_shipper_no');
            $table->string('photo');
			$table->text('notes');
			$table->timestamps();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
