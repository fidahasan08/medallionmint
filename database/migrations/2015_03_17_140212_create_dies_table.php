<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dies', function(Blueprint $table)
        {
            $table->increments('id');
            $table->tinyInteger('active')->default(1);
            $table->string('name');
            $table->integer('location_id');
            $table->string('cabinet'); 	//A,B,C,D
            $table->integer('drawer'); 	//1,2,3......8
            $table->string('row'); 		//A,B...G
            $table->integer('col');		//1,2.....6
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dies');
    }

}
