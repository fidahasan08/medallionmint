<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('machines', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('active')->default(1);
			
			$table->integer('location_id')->unsigned();
			$table->string('name');
			$table->string('photo');
			$table->string('specific_location');
			$table->string('asset_num_1');
                  $table->string('asset_num_2');
                  $table->date('date_shipped');
                  $table->date('date_installed');

                  $table->integer('machine_types');
                  $table->integer('machine_colors');
                  $table->integer('machine_keys');
                  $table->integer('prices');
                  $table->integer('motors');
                  $table->integer('button_lights');

                  /*Coins*/
                  $table->integer('coins');
                  $table->integer('coin_1');
                  $table->integer('coin_2');
                  $table->integer('coin_3');
                  $table->integer('coin_4');

                  /*Gizmo*/
                  $table->integer('gizmo_models');
                  $table->string('gizmo_serial');
                  $table->integer('meter_box_models');
                  $table->string('meter_box_serial');
                  $table->integer('gizmo_fws');
                  $table->string('gizmo_pic');
                  
                  /*Credit Card*/
                  $table->boolean('has_cc');
                  $table->integer('cc_companies');
                  $table->integer('cc_antennas');
                  $table->integer('cc_modem_models');
                  $table->integer('cc_reader_models');
                  $table->string('cc_modem_serial');
                  $table->string('cc_reader_serial');

                  /*Bill Validator*/
                  $table->integer('bill_validator_models');
                  $table->integer('vault_models');
                  $table->integer('vault_keys');
                  $table->string('bill_validator_serial');
                  $table->string('vault_serial');

                  /*Hopper*/
                  $table->integer('hopper_models_1');
                  $table->integer('hopper_models_2');
                  $table->integer('hopper_models_3');
                  $table->integer('hopper_models_4');

                  $table->string('hopper_serial_1');
                  $table->string('hopper_serial_2');
                  $table->string('hopper_serial_3');
                  $table->string('hopper_serial_4');

                  /*Meters*/
                  $table->integer('current_meter_1');
                  $table->integer('current_meter_2');
                  $table->integer('current_meter_3');
                  $table->integer('current_meter_4');
                  $table->integer('current_meter_5');
                  $table->integer('current_meter_6');

                  $table->integer('starting_meter_1');
                  $table->integer('starting_meter_2');
                  $table->integer('starting_meter_3');
                  $table->integer('starting_meter_4');
                  $table->integer('starting_meter_5');
                  $table->integer('starting_meter_6');

                  /*Shipped*/
                  $table->integer('shipped_meter_1');
                  $table->integer('shipped_meter_2');
                  $table->integer('shipped_meter_3');
                  $table->integer('shipped_meter_4');
                  $table->integer('shipped_meter_5');
                  $table->integer('shipped_meter_6');

                  $table->integer('shipped_coin_1_id');
                  $table->integer('shipped_coin_2_id');
                  $table->integer('shipped_coin_3_id');
                  $table->integer('shipped_coin_4_id');

                  $table->integer('shipped_coin_1_quantity');
                  $table->integer('shipped_coin_2_quantity');
                  $table->integer('shipped_coin_3_quantity');
                  $table->integer('shipped_coin_4_quantity');           


			$table->timestamps();

			$table->foreign('location_id')
                              ->references('id')
                              ->on('locations')
                              ->onDelete('cascade');

                  

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('machines');
	}

}
