<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachineReadingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('machine_readings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('report_id')->unsigned();
			$table->integer('machine_id')->unsigned();

			$table->integer('meter_1_previous');
			$table->integer('meter_2_previous');
			$table->integer('meter_3_previous');
			$table->integer('meter_4_previous');
			$table->integer('meter_5_previous');
			$table->integer('meter_6_previous');

			$table->integer('meter_1_current');
			$table->integer('meter_2_current');
			$table->integer('meter_3_current');
			$table->integer('meter_4_current');
			$table->integer('meter_5_current');
			$table->integer('meter_6_current');

			$table->integer('meter_1_adj');
			$table->integer('meter_2_adj');
			$table->integer('meter_3_adj');
			$table->integer('meter_4_adj');
			$table->integer('meter_5_adj');
			$table->integer('meter_6_adj');

			$table->integer('meter_3_display');
			$table->integer('meter_4_display');
			$table->integer('meter_5_display');
			$table->integer('meter_6_display');

			$table->integer('meter_3_stock');
			$table->integer('meter_4_stock');
			$table->integer('meter_5_stock');
			$table->integer('meter_6_stock');

			$table->timestamps();
			$table->foreign('report_id')
                          ->references('id')
                          ->on('reports')
                          ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('machine_readings');
	}

}
