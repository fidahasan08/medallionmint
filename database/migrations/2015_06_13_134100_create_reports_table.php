<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('location_id')->unsigned();
			$table->integer('prev_report')->unsigned();
			$table->string('type');
			$table->string('month');
			$table->string('year');
			$table->date('date');
			$table->time('time');
			$table->string('read_by');
			$table->string('verified_by');

			$table->timestamps();
			$table->foreign('location_id')
                          ->references('id')
                          ->on('locations')
                          ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reports');
	}

}
