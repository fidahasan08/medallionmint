<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('companies')->delete();

        $companies = array(
            array('id' => 1, 'name' => 'Medallion Mint', 'address' => '1201 S 3rd St', 'website' => 'medallionmint.com'),
            array('id' => 2, 'name' => 'CTM Group, Inc.', 'address' => 'somewhere', 'website' => 'ctmgroupinc.com'),
            array('id' => 3, 'name' => 'Characters Unlimited', 'address' => 'somewhere', 'website' => 'charactersunlimited.com')
        );

        DB::table('companies')->insert($companies);
    }
}