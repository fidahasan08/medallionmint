<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    public function run()
    {
   
        $this->call('UsersTableSeeder');
        $this->call('CompaniesTableSeeder');        
        $this->call('LocationsTableSeeder');          
        $this->call('CoinsTableSeeder');
        $this->Call('DiesTableSeeder');       
       
    }

}