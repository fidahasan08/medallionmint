<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('users')->delete();

        $users = array(
            array('id' => 1, 'name' => 'Fida Hasan', 'email' => 'fidahasan08@gmail.com', 'password' => Hash::make('123'), 'admin' => 1),
            array('id' => 2, 'name' => 'Sumu Hasan', 'email' => 'sumuhasan08@gmail.com', 'password' => Hash::make('123'), 'admin' => 0),
            array('id' => 3, 'name' => 'Tim McClure', 'email' => 'tim@medallionmint.com', 'password' => Hash::make('123'), 'admin' => 1),
            array('id' => 4, 'name' => 'Pam McClure', 'email' => 'pam@medallionmint.com', 'password' => Hash::make('123'), 'admin' => 0)
    );

        DB::table('users')->insert($users);
    }
}