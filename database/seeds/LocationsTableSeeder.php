<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('locations')->delete();

        $locations = array(
            array('id' => 1, 'active' => 1, 'company_id' => 1, 'name' => 'The Alamo', 'photo' => 'ykb7pnMTSgXPAlamo.jpg'),
            array('id' => 2, 'active' => 1, 'company_id' => 1, 'name' => 'Statue of Liberty', 'photo' => ''),
            array('id' => 3, 'active' => 1, 'company_id' => 1, 'name' => 'Adventure Aquarium', 'photo' => '4FMnYEOI1rhSAdventure Aquarium.jpg'),
            array('id' => 4, 'active' => 1, 'company_id' => 2, 'name' => 'Akron Zoo', 'photo' => 'xlxC0pfyaTE1Akron Zoo.jpg'),
            array('id' => 5, 'active' => 1, 'company_id' => 1, 'name' => 'Bass Pro Branson', 'photo' => ''),
            array('id' => 6, 'active' => 1, 'company_id' => 1, 'name' => 'Ellis Island', 'photo' => ''),
            array('id' => 7, 'active' => 1, 'company_id' => 1, 'name' => 'Mount Rushmore', 'photo' => ''),
            array('id' => 8, 'active' => 1, 'company_id' => 1, 'name' => 'Graceland', 'photo' => ''),
            array('id' => 9, 'active' => 1, 'company_id' => 1, 'name' => 'Hoover Dam Store', 'photo' => ''),
            array('id' => 10, 'active' => 1, 'company_id' => 2, 'name' => 'LegoLand Ca', 'photo' => ''),
            array('id' => 11, 'active' => 1, 'company_id' => 2, 'name' => 'LegoLand NY', 'photo' => ''),
            array('id' => 12, 'active' => 1, 'company_id' => 1, 'name' => 'Natural Bridge Caverns', 'photo' => ''),
            array('id' => 13, 'active' => 1, 'company_id' => 1, 'name' => 'North Caroline Zoo', 'photo' => ''),
            array('id' => 14, 'active' => 1, 'company_id' => 1, 'name' => 'Ripley\'s BON Baltimore', 'photo' => ''),
            array('id' => 15, 'active' => 1, 'company_id' => 1, 'name' => 'Ripley\'s BON Key West', 'photo' => ''),
            array('id' => 16, 'active' => 1, 'company_id' => 3, 'name' => 'Kalahari Sandusky', 'photo' => ''),
            array('id' => 17, 'active' => 1, 'company_id' => 3, 'name' => 'Kalahari Wisconsin Dells', 'photo' => ''),
            array('id' => 18, 'active' => 1, 'company_id' => 3, 'name' => 'Auto Car Collection', 'photo' => '')
        );

        DB::table('locations')->insert($locations);
    }

}