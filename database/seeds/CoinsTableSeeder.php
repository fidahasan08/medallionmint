<?php

use Illuminate\Database\Seeder;

class CoinsTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('coins')->delete();

        $coins = array(
            array('id' 				=> 1, 
            	'name' 				=> 'Lone Star State', 
            	'location_id' 		=> 1, 
            	'top_inventory' 	=> '125' , 
            	'inventory' 		=> '500' , 
            	'metal' 			=> 'Brass', 
            	'finish' 			=> 'Shiny', 
            	'front_dies_type' 	=> 'Location Dies', 
            	'back_dies_type' 	=> 'Location Dies', 
            	'front_dies' 		=> 2, 
            	'back_dies' 		=> 3),
            array('id' 				=> 2, 
            	'name' 				=> 'Test Coin 2', 
            	'location_id' 		=> 1, 
            	'top_inventory' 	=> '125' , 
            	'inventory' 		=> '1500' , 
            	'metal' 			=> 'Brass', 
            	'finish' 			=> 'Shiny', 
            	'front_dies_type' 	=> 'Location Dies', 
            	'back_dies_type' 	=> 'Location Dies', 
            	'front_dies' 		=> 2, 
            	'back_dies' 		=> 3)
    );

        DB::table('coins')->insert($coins);
    }
}