<?php

use Illuminate\Database\Seeder;

class DiesTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('dies')->delete();

        $dies = array(
            array('id' => 1, 'name' => 'Polar Bear', 'location_id' => 0, 'cabinet'=>'A', 'drawer' => '1', 'row'=>'B','col'=>'2', 'photo' => 'TqxPveEcZ2T2_polar_bear.png'),
            array('id' => 2, 'name' => 'State of Texas', 'location_id' => 1, 'cabinet'=>'B', 'drawer' => '2', 'row'=>'C','col'=>'3','photo' => 'MwfTIpQx75vx_alamo-stateoftexas.png'),
            array('id' => 3, 'name' => 'Lone Star State', 'location_id' => 1, 'cabinet'=>'C', 'drawer' => '3', 'row'=>'D','col'=>'4','photo' => 'FTPLQNoE8H73_alamo-lonestarstate.png'),
    );

        DB::table('dies')->insert($dies);
    }
}