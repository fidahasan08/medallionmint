<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{


    protected $fillable = ['active','name', 'address', 'website', 'notes'];


    public function location()
    {

        return $this->hasMany('App\Location');
    }

    public function machine()
    {

        return $this->hasMany('App\Machine');
    }

}


