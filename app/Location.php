<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

	protected $fillable = ['active','company_id','name', 'address','website', 'shipping_method','corporate_shipper_no','notes','photo'];

    public function managers(){
        return $this->belongsToMany('App\User','location_user')->withTimestamps();
    }

	public function company(){

		return $this->belongsTo('App\Company');
	}

    public function machine(){
        return $this->hasMany('App\Machine');
    }

    public function report(){
        return $this->hasMany('App\Report');
    }

    public function coins(){
        return $this->hasMany('App\Coin');
    }

    public function dies(){
        return $this->hasMany('App\Dies');
    }
    public function front_dies(){

        return Dies::where('id','=',$this->front_dies)->first();
    }

    public function back_dies(){

        return Dies::where('id','=',$this->back_dies)->first();
    }
}

