<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		$user = \App\User::find($this->users);

		if($this->isMethod('PATCH')){

            $rules = [

                'name' => 'required',
				'email' => 'required|email|unique:users,email,'.$user->id,
				'password'=> 'min:3'
            ];

        }else{

            $rules = [

                'name' => 'required',
				'email' => 'required|email|unique:users,email',
				'password'=> 'required|min:3'
            ];

        }

		return $rules;
	}

}
