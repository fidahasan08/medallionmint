<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class MachineRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'location_id' => 'required',
            'name' => 'required',
            'specific_location' => 'required',
            'asset_num_1' => 'required',
            'asset_num_2' => 'required',
            'date_shipped' => 'required|date',
            'date_installed' => 'required|date'
		];
	}

}
