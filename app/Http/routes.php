<?php
use App\User;
use App\Dies;


Route::get('/', function(){

	return redirect('dashboard');
});

Route::get('home', function()
{
    return redirect('dashboard');
});

Route::group(['middleware' => 'auth'], function()
{
	Route::get('dashboard', function(){
		
        $user = Auth::user();
        if($user->locations->count()){
            $location = $user->locations->first();
            return view('dashboard',compact('location'));
        }else{

            return view('no_permission',compact('user'));
        }
        
	});

    Route::get('reports/create/{type}','ReportsController@create');
    Route::get('reports/{report_id}/{action}','ReportsController@show');    

    Route::resource('reports','ReportsController');
    Route::resource('machines','MachinesController');
    Route::resource('dies','DiesController');
    Route::resource('coins','CoinsController');


});


Route::group(['prefix' => 'admin', 'middleware'=>'admin'], function()
{

    Route::get('dashboard', function()    {
        return view('admin.dashboard');
    });   


    //Route::post('users/{user_id}/attach/{location_id}','admin\AdminUsersController@attach');
    Route::post('users/attach','admin\AdminUsersController@attach');
    Route::post('users/{user_id}/remove/{location_id}','admin\AdminUsersController@remove');

    Route::resource('users','admin\AdminUsersController');
    
    Route::resource('companies','admin\AdminCompaniesController');
    Route::resource('locations','admin\AdminLocationsController');

    Route::get('locations/{location_id}/reports/create/{type}','admin\AdminReportsController@create');
    Route::get('locations/{location_id}/reports/{report_id}/edit','admin\AdminReportsController@edit');    
    Route::get('locations/{location_id}/reports/{report_id}/{action}','admin\AdminReportsController@show');    
    Route::resource('locations.reports','admin\AdminReportsController');    

    Route::resource('machines','admin\AdminMachinesController');
    Route::resource('machine_settings','admin\AdminMachineSettingsController');
    Route::resource('dies','admin\AdminDiesController');
    Route::resource('coins','admin\AdminCoinsController');
});



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('get_dies/{location_id}', function($location_id){
    return \DB::table('dies')->where('location_id', $location_id)->get();

});

Route::get('get_coins/{location_id}', function($location_id){
    return \DB::table('coins')->where('location_id', $location_id)->get();

});

