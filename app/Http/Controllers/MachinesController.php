
<?php namespace App\Http\Controllers;

use App\Machine;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class MachinesController extends Controller {

	
	public function index()
    {
        $user = \Auth::user();
        if($user->locations->count()){
            $location = $user->locations->first();
            $machines = $location->machine;
            
        	return view('machines.index',compact('machines','location'));
        }else{

            return view('no_permission',compact('user'));
        }


    }

	

}
