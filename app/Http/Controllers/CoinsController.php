<?php namespace App\Http\Controllers;

use App\Coin;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CoinsController extends Controller {

	
	public function index()
    {
        $user = \Auth::user();
        if($user->locations->count()){
            $location = $user->locations->first();
            $coins = $location->coins;
            
        	return view('coins.index',compact('coins','location'));
        }else{

            return view('no_permission',compact('user'));
        }


    }

	

}
