<?php namespace App\Http\Controllers\admin;

use App\Location;
use App\Machine;
use App\MachineSettings;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\MachineRequest;

use Illuminate\Http\Request;

use File;
use Image;

class AdminMachinesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$machines = Machine::all();		
		return view('admin.machines.index',compact('machines'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id');
        
        $machine_settings = MachineSettings::all()->groupBy('setting');        

		return view('admin.machines.create')->with([
				'locations'=>$locations,
				'machine_settings' => $machine_settings
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MachineRequest $request)
	{
		$request['date_shipped'] = date('Y-m-d',strtotime($request['date_shipped']));
		$request['date_installed'] = date('Y-m-d',strtotime($request['date_installed']));


		$file = $request->file('photo');

		if($file) {

			$filename = str_random(12);
			$filename .= '_' . $file->getClientOriginalName();
			$destinationPath = public_path() . '/upload/';
			$file->move($destinationPath, $filename);


			$img = Image::make($destinationPath.$filename);

			$img->resize(400, null, function($constraint){$constraint->aspectRatio();})
				->save($destinationPath.'400-'.$filename);

			$img->resize(50, null, function($constraint){$constraint->aspectRatio();})
				->save($destinationPath.'50-'.$filename);

			$img->destroy();


			Machine::create($request->except('photo')+array('photo'=>$filename));

		}else{

			Machine::create($request->all());
		}

		return redirect()->back()->with([
            'flash_message' => 'New Machine created!'
        ]);
                
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	
		$machine = Machine::findOrFail($id);
		$locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id');        
        $machine_settings = MachineSettings::all()->groupBy('setting');        

        return view('admin.machines.edit')->with(array('machine'=>$machine,'locations'=>$locations,'machine_settings'=>$machine_settings));
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, MachineRequest $request)
	{
		$machine = Machine::findOrFail($id);

		$file = $request->file('photo');

		if($file) {

			File::delete(public_path() . '/upload/' . $machine->photo);
			File::delete(public_path() . '/upload/50-' . $machine->photo);
			File::delete(public_path() . '/upload/400-' . $machine->photo);

			$filename = str_random(12);
			$filename .= $file->getClientOriginalName();
			$destinationPath = public_path().'/upload/';
			$file->move($destinationPath, $filename);

			$img = Image::make($destinationPath.$filename);

			$img->resize(400, null, function($constraint){$constraint->aspectRatio();})
				->save($destinationPath.'400-'.$filename);

			$img->resize(50, null, function($constraint){$constraint->aspectRatio();})
				->save($destinationPath.'50-'.$filename);

			$img->destroy();

			$machine->update($request->except('photo')+array('photo'=>$filename));

		}else{

			$machine->update($request->all());
		}

		return redirect()->back()->with([
				'flash_message'=>'Machine Updated Successfully'

			]);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$machine = Machine::findOrFail($id);
		$machine->delete();

		return response('success');
	}

}
