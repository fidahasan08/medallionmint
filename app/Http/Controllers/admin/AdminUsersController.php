<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User;
use App\Location;
use \Hash;

class AdminUsersController extends Controller {

	
	public function index()
	{
		$locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name','id');
		$users = User::orderBy('admin','desc')->orderby('created_at','desc')->get();
        return view('admin.users.index',compact('users','locations'));
	}

	
	public function create()
	{
		$locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name','id');
		return view('admin.users.create',compact('locations'));
	}

	
	public function store(UserRequest $request)
	{
		//return $request->all();

		$request->merge(['password' => Hash::make($request->password)]);
		$user = User::create($request->all());

		$user->locations()->attach($request['location_id']);

		return redirect()->back()->with([
			'flash_message' => 'New Location Manager Created'
			]);
	}

	
	public function show($id)
	{
		//
	}

	
	public function edit($id)
	{
		$user = User::findOrFail($id);
		return view('admin.users.edit',compact('user'));
	}

	
	public function update($id,UserRequest $request)
	{
		$user = User::findOrFail($id);

		if($request['password']==''){
			$user->update($request->except('password'));
		}else{
			//$dies->update($request->except('photo')+array('photo'=>$filename));
			$user->update($request->except('password')+array('password'=>Hash::make($request['password'])));
		}

		return redirect('admin/users')->with([
				'flash_message' => $user->name.' user info updated'
			]);

	}
	
	public function attach(Request $data){
		$user = User::findOrFail($data['user_id']);
		//$user->locations()->attach($data['location_id']);
		$user->locations()->sync($data['location_id'], false);

		return redirect()->back()->with([
			'flash_message' => 'Location Access updated'
			]);

	}


	public function remove($user_id,$location_id){
		
		$user = User::findOrFail($user_id);
		$user->locations()->detach($location_id);
		return response('success');
	}


	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$user->delete();

		return response('success');
	}

}
