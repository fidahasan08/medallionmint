<?php namespace App\Http\Controllers\admin;

use App\Company;
use App\Location;
use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;

class AdminCompaniesController extends Controller {

	
	public function index()
	{	
				
		$companies = Company::where('active','=','1')->orderBy('name', 'asc')->get();
		return view('admin.companies.index',compact('companies'));
		
	}

	
	public function create()
	{
		return view('admin.companies.create');
	}

	
	public function store(CompanyRequest $request)
	{
		//Company::create(Request::all());
		Company::create($request->all());
		return redirect('admin/companies')->with([
            'flash_message' => '<b>' . $request['name'] . '</b> created!'
        ]);
	}

	
	public function show($id)
	{
		$company = Company::where('active','=','1')->orderBy('name', 'asc')->findOrFail($id);
		$locations = Location::where('active','=','1')->where('company_id','=',$id)->orderBy('name', 'asc')->get();
        //$machine = Machine::where()

		return view('admin.companies.show')->with(array(
            'company' => $company,
            'locations' => $locations
        ));
	}

	
	public function edit($id)
	{
		$company = Company::findOrFail($id);		

		return view('admin.companies.edit',compact('company'));
	}

	
	public function update($id, CompanyRequest $request)
	{
		$company = Company::findOrFail($id);
		$company->update($request->all());

		return redirect('admin/companies')->with([
            'flash_message' => '<b>' . $request['name'] . '</b> updated!'
        ]);
	}

	
	public function destroy($id)
	{
		
		$company = Company::findOrFail($id);
		$company->delete();

		return response('success');
	}

}
