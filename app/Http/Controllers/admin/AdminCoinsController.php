<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CoinRequest;
use App\Coin;
use App\Location;
use App\Dies;

class AdminCoinsController extends Controller {


    public function index()
    {
        $coins = Coin::orderBy('created_at','desc')->get();
        return view('admin.coins.index',compact('coins'));
    }


    public function create()
    {
        $locations = array(''=>'Please Select')+Location::lists('name', 'id');
        return view('admin.coins.create',compact('locations'));
    }


    public function store(CoinRequest $request)
    {

        Coin::create($request->all());
        return redirect('admin/coins');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $coin = Coin::findOrFail($id);


        $locations = array(''=>'Please Select')+Location::lists('name', 'id');

        //$location_dies = Dies::where('location_id','!=',0)->lists('name','id');
        //$stock_dies = Dies::where('location_id','=',0)->lists('name','id');

        $location_dies = Dies::where('location_id','!=',0)->get();
        $stock_dies = Dies::where('location_id','=',0)->get();


        return view('admin.coins.edit')->with(array(
            'coin'=>$coin,
            'locations'=>$locations,
            'location_dies' => $location_dies,
            'stock_dies'=>$stock_dies
        ));
    }


    public function update($id,CoinRequest $request)
    {
        $coin = Coin::findOrFail($id);
        $coin->update($request->all());

        return redirect('admin/coins')->with('success','Coin has been updated successfully');
    }


    public function destroy($id)
    {
        $coin = Coin::findOrFail($id);
        $coin->delete();

        return response('success');
    }

}
