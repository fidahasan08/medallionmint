<?php namespace App\Http\Controllers\admin;
use App\Location;
use App\Dies;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\DiesRequest;
use Illuminate\Http\Request;

use Image;
use File;

class AdminDiesController extends Controller {


    public function index()
    {
        $dies = Dies::orderBy('created_at','desc')->get();
        return view('admin.dies.index',compact('dies'));
    }


    public function create()
    {

        $locations = array(0=>'Stock Die')+Location::lists('name', 'id');
        return view('admin.dies.create',compact('locations'));
    }


    public function store(DiesRequest $request)
    {
        $file = $request->file('photo');

        $filename = str_random(12);
        $filename .= '_'.$file->getClientOriginalName();
        $destinationPath = public_path().'/upload/';
        $file->move($destinationPath, $filename);



        $img = Image::make($destinationPath.$filename);

        $img->resize(200, null, function ($constraint) {  $constraint->aspectRatio();})
            ->save($destinationPath.'200-'.$filename);

        $img->resize(100, null, function ($constraint) {  $constraint->aspectRatio();})
            ->save($destinationPath.'100-'.$filename);

        $img->resize(50, null, function ($constraint) {  $constraint->aspectRatio();})
            ->save($destinationPath.'50-'.$filename);


        $img->destroy();

        Dies::create($request->except('photo')+array('photo'=>$filename));


        return redirect('admin/dies')->with('success','New Dies has been created successfully');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $dies = Dies::findOrFail($id);
        $locations = array(0=>'Stock Die')+Location::lists('name', 'id');
        return view('admin.dies.edit')->with(array('dies'=>$dies,'locations'=>$locations));
    }


    public function update($id,DiesRequest $request)
    {
        $dies = Dies::findOrFail($id);

        $file = $request->file('photo');

        if($file){

            File::delete(public_path().'/upload/'.$dies->photo);
            File::delete(public_path().'/upload/200-'.$dies->photo);
            File::delete(public_path().'/upload/100-'.$dies->photo);
            File::delete(public_path().'/upload/50-'.$dies->photo);

            /************************************************/
            $filename = str_random(12);
            $filename .= '_'.$file->getClientOriginalName();
            $destinationPath = public_path().'/upload/';
            $file->move($destinationPath, $filename);



            $img = Image::make($destinationPath.$filename);

            $img->resize(200, null, function ($constraint) {  $constraint->aspectRatio();})
                ->save($destinationPath.'200-'.$filename);

            $img->resize(100, null, function ($constraint) {  $constraint->aspectRatio();})
                ->save($destinationPath.'100-'.$filename);

            $img->resize(50, null, function ($constraint) {  $constraint->aspectRatio();})
                ->save($destinationPath.'50-'.$filename);


            $img->destroy();



            $dies->update($request->except('photo')+array('photo'=>$filename));

        }else{

            $dies->update($request->all());
        }

        return redirect('admin/dies')->with('success','Dies has been updated successfully');


    }


    public function destroy($id)
    {
        $dies = Dies::findOrFail($id);

        File::delete(public_path().'/upload/'.$dies->photo);
        File::delete(public_path().'/upload/200-'.$dies->photo);
        File::delete(public_path().'/upload/100-'.$dies->photo);
        File::delete(public_path().'/upload/50-'.$dies->photo);

        $dies->delete();

        return response('success');
    }

}
