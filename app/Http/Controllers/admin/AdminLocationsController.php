<?php namespace App\Http\Controllers\admin;

use App\Company;
use App\Coin;
use App\Dies;
use App\Machine;
use App\Location;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\LocationRequest;
use Illuminate\Http\Request;

use Image;
use File;

class AdminLocationsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$locations = Location::where('active','=','1')->orderBy('name', 'asc')->get();
		return view('admin.locations.index',compact('locations'));
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$companies = Company::orderBy('name','desc')->get();
		$companies = Company::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id');
		return view('admin.locations.create',compact('companies'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(LocationRequest $request)
	{
        $file = $request->file('photo');

		if($file) {

	        $filename = str_random(12);
	        $filename .= '_' . $file->getClientOriginalName();
	        $destinationPath = public_path() . '/upload/';
	        $file->move($destinationPath, $filename);


	        $img = Image::make($destinationPath.$filename);

	        $img->resize(400, null, function($constraint){$constraint->aspectRatio();})
	            ->save($destinationPath.'400-'.$filename);

	        $img->resize(200,null, function($constraint){$constraint->aspectRatio();})
	            ->save($destinationPath.'200-'.$filename);

	        $img->resize(75, null, function($constraint){$constraint->aspectRatio();})
	            ->save($destinationPath.'75-'.$filename);

	        $img->destroy();


			Location::create($request->except('photo')+array('photo'=>$filename));

		}else{

			$location->update($request->all());
		}


		return redirect('admin/locations')->with([
            'flash_message' => '<b>' . $request['name'] . '</b> created!'
        ]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 * @return Response
	 */
	public function show($id)
	{
		$location = Location::findOrFail($id);

		

		if($location->report->count()){
            $last_report_created_ago = round( (time() - strtotime($location->report()->orderBy('created_at','desc')->first()->created_at)) / (3600*24) );
        }else{
        	$last_report_created_ago = 99999999;
        }        

		return view('admin.locations.show',compact('location','last_report_created_ago'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$location = Location::findOrFail($id);		
		$companies = Company::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id');

		return view('admin.locations.edit')->with(array('location'=>$location,'companies'=>$companies));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, LocationRequest $request)
	{
		$location = Location::findOrFail($id);

        $file = $request->file('photo');

        if($file) {

            File::delete(public_path() . '/upload/' . $location->photo);
            File::delete(public_path() . '/upload/75-' . $location->photo);
            File::delete(public_path() . '/upload/200-' . $location->photo);
            File::delete(public_path() . '/upload/400-' . $location->photo);

            $filename = str_random(12);
            $filename .= $file->getClientOriginalName();
            $destinationPath = public_path().'/upload/';
            $file->move($destinationPath, $filename);

            $img = Image::make($destinationPath.$filename);

            $img->resize(400, null, function($constraint){$constraint->aspectRatio();})
                ->save($destinationPath.'400-'.$filename);

            $img->resize(200,null, function($constraint){$constraint->aspectRatio();})
                ->save($destinationPath.'200-'.$filename);

            $img->resize(75, null, function($constraint){$constraint->aspectRatio();})
                ->save($destinationPath.'75-'.$filename);

            $img->destroy();

            $location->update($request->except('photo')+array('photo'=>$filename));

        }else{

            $location->update($request->all());
        }


		return redirect('admin/locations')->with([
            'flash_message'=> '<b>' . $request['name'] . '</b> updated!'
        ]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$location = Location::findOrFail($id);

        File::delete(public_path().'/upload/'.$location->photo);
        File::delete(public_path().'/upload/75-'.$location->photo);
        File::delete(public_path().'/upload/100-'.$location->photo);
        File::delete(public_path().'/upload/300-'.$location->photo);

		$location->delete();

		return response('success');
	}

}
