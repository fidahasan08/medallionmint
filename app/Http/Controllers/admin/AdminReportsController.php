<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use Illuminate\Http\Request;
use App\Location;
use App\Machine;
use App\Report;
use App\MachineReading;
use \PDF;


class AdminReportsController extends Controller {

	
	public function index($location_id)	
	{
		
	}

	
	public function create($location_id,$type='monthly')
	{
		
		$location = Location::findOrFail($location_id);
		$machines = $location->machine;		

		if($type=='monthly'){
			$prev_report = Report::where('location_id',$location_id)
									->where('type','monthly')
									->orderBy('created_at','desc')->first();
		}else{
			$prev_report = Report::where('location_id',$location_id)
									->orderBy('created_at','desc')->first();
		}	

		return view('admin.reports.create',compact('location','machines','type','prev_report'));
	}

	
	public function store($location_id,ReportRequest $request)
	{		
		$request['location_id'] = $location_id;
		$request['date'] = date('Y-m-d',strtotime($request['date']));
		$report = Report::create($request->all());

		//return $request->all();
		
		for($i=1;$i<=$request['no_of_machines'];$i++){
			$machine_reading = new MachineReading;
			$machine_reading->report_id 	= $report->id;
			$machine_reading->machine_id 	= $request['machine_'.$i.'_id'];

			for($j=1;$j<=($request['machine_'.$i.'_no_of_coins']+2);$j++){
				$machine_reading->{'meter_'.$j.'_previous'}	= $request['machine_'.$i.'_meter_'.$j.'_previous'];
				$machine_reading->{'meter_'.$j.'_current'}	= $request['machine_'.$i.'_meter_'.$j.'_current'];
				$machine_reading->{'meter_'.$j.'_adj'}	= $request['machine_'.$i.'_meter_'.$j.'_adj'];

				if($j>2){
					$machine_reading->{'meter_'.$j.'_display'}	= $request['machine_'.$i.'_meter_'.$j.'_display'];
					$machine_reading->{'meter_'.$j.'_stock'}	= $request['machine_'.$i.'_meter_'.$j.'_stock'];
				}
			}

			$machine_reading->save();

			//next: update current meter of machine..
			//.........
		}
		
		return redirect('admin/locations/'.$location_id)->with([
				'flash_message' => 'New report created'
			]);

		
	}

	
	public function show($location_id,$report_id,$action='view')
	{
		
		$report = Report::findOrFail($report_id);

		$data['cash_gross_total'] = 0;
		$data['cash_adj_total'] = 0;		

		$data['cc_gross_total'] = 0;
		$data['cc_adj_total'] = 0;

		$data['total_coins'] = 0;

		foreach($report->machine_readings as $machine_reading){                       
	        
	        	
        	$data['cash_gross_total'] += $machine_reading->meter_1_current - $machine_reading->meter_1_previous;
        	$data['cash_adj_total'] += $machine_reading->meter_1_adj;

        	$data['cc_gross_total'] += $machine_reading->meter_2_current - $machine_reading->meter_2_previous;
        	$data['cc_adj_total'] += $machine_reading->meter_2_adj; 

        	for($i=1;$i<=$machine_reading->machine()->coins;$i++){
        		$data['total_coins'] += $machine_reading->{'meter_'.($i+2).'_current'} - $machine_reading->{'meter_'.($i+2).'_previous'} + $machine_reading->{'meter_'.($i+2).'_adj'};
        	}
	             
	        
	    }

	    $data['cash_sub_total'] = $data['cash_gross_total'] + $data['cash_adj_total'];
	    $data['cc_sub_total'] = $data['cc_gross_total'] + $data['cc_adj_total'];

	    $data['total_sales'] = $data['cash_sub_total'] + $data['cc_sub_total'] ;

	    $data['cash_percent'] = round($data['cash_sub_total'] / $data['total_sales'] * 100);
	    $data['cc_percent'] = round($data['cc_sub_total'] / $data['total_sales'] * 100);

	    $data['per_coin'] = $data['total_sales'] / $data['total_coins'];
			
		
	    $pdf = PDF::loadView('admin.reports.pdf', compact('report','data'));
	    
	    $file_name = str_slug($report->location->name,'_').'_'.$report->type.'_report_'.$report->id.'.pdf';
	    
	    if($action=='download'){
	    	return $pdf->download($file_name);		
	    }
	    
	    return $pdf->stream($file_name);		
		
	}

	
	public function edit($location_id,$report_id)
	{
		$report = Report::findOrFail($report_id);
		$location = Location::findOrFail($location_id);
		$machines = $location->machine;

		if($report->prev_report){
			$prev_report = Report::findOrFail($report->prev_report);
		}else{
			$prev_report = 0 ;
		}

		return view('admin.reports.edit',compact('report','machines','location', 'prev_report'));
	}

	
	public function update($location_id,$report_id,ReportRequest $request)
	{
	
		$request['location_id'] = $location_id;
		$request['date'] = date('Y-m-d',strtotime($request['date']));
		
		$report = Report::findOrFail($report_id);
		$report->update($request->all());

		MachineReading::where('report_id',$report_id)->delete();

		for($i=1;$i<=$request['no_of_machines'];$i++){
			$machine_reading = new MachineReading;
			$machine_reading->report_id 	= $report->id;
			$machine_reading->machine_id 	= $request['machine_'.$i.'_id'];

			for($j=1;$j<=($request['machine_'.$i.'_no_of_coins']+2);$j++){
				$machine_reading->{'meter_'.$j.'_previous'}	= $request['machine_'.$i.'_meter_'.$j.'_previous'];
				$machine_reading->{'meter_'.$j.'_current'}	= $request['machine_'.$i.'_meter_'.$j.'_current'];
				$machine_reading->{'meter_'.$j.'_adj'}	= $request['machine_'.$i.'_meter_'.$j.'_adj'];

				if($j>2){
					$machine_reading->{'meter_'.$j.'_display'}	= $request['machine_'.$i.'_meter_'.$j.'_display'];
					$machine_reading->{'meter_'.$j.'_stock'}	= $request['machine_'.$i.'_meter_'.$j.'_stock'];
				}
			}

			$machine_reading->save();

			//next: update current meter of machine..
			//.........
		}

		return redirect()->back()->with([
				'flash_message'=>'Report Updated Successfully'
			]);
		

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($location_id,$report_id)
	{
		$report = Report::findOrFail($report_id);
		$report->delete();

		return response('success');
	}

}
