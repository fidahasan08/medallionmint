<?php namespace App\Http\Controllers;

use App\Report;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\ReportRequest;
use App\MachineReading;
use \PDF;

class ReportsController extends Controller {

	public function index()
	{
		$user = \Auth::user();
        if($user->locations->count()){
            
            $location = $user->locations->first();
            
            if($location->report->count()){
	            $last_report_created_ago = round( (time() - strtotime($location->report()->orderBy('created_at','desc')->first()->created_at)) / (3600*24) );
	        }else{
	        	$last_report_created_ago = 99999999;
	        }      
            
        	return view('reports.index',compact('location','last_report_created_ago'));

        }else{

            return view('no_permission',compact('user'));
        }

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($type='monthly')
	{
		$user = \Auth::user();
        if($user->locations->count()){            
            $location = $user->locations->first();           
        }else{

            return view('no_permission',compact('user'));
        }

		
		$machines = $location->machine;		

		if($type=='monthly'){
			$prev_report = Report::where('location_id',$location->id)
									->where('type','monthly')
									->orderBy('created_at','desc')->first();
		}else{
			$prev_report = Report::where('location_id',$location->id)
									->orderBy('created_at','desc')->first();
		}	

		return view('reports.create',compact('location','machines','type','prev_report'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ReportRequest $request)
	{		
		$user = \Auth::user();
        if($user->locations->count()){            
            $location = $user->locations->first();           
        }else{

            return view('no_permission',compact('user'));
        }

		$request['location_id'] = $location->id;
		$request['date'] = date('Y-m-d',strtotime($request['date']));
		$report = Report::create($request->all());

		//return $request->all();
		
		for($i=1;$i<=$request['no_of_machines'];$i++){
			$machine_reading = new MachineReading;
			$machine_reading->report_id 	= $report->id;
			$machine_reading->machine_id 	= $request['machine_'.$i.'_id'];

			for($j=1;$j<=($request['machine_'.$i.'_no_of_coins']+2);$j++){
				$machine_reading->{'meter_'.$j.'_previous'}	= $request['machine_'.$i.'_meter_'.$j.'_previous'];
				$machine_reading->{'meter_'.$j.'_current'}	= $request['machine_'.$i.'_meter_'.$j.'_current'];
				$machine_reading->{'meter_'.$j.'_adj'}	= $request['machine_'.$i.'_meter_'.$j.'_adj'];

				if($j>2){
					$machine_reading->{'meter_'.$j.'_display'}	= $request['machine_'.$i.'_meter_'.$j.'_display'];
					$machine_reading->{'meter_'.$j.'_stock'}	= $request['machine_'.$i.'_meter_'.$j.'_stock'];
				}
			}

			$machine_reading->save();

			//next: update current meter of machine..
			//.........
		}
		
		return redirect('reports')->with([
				'flash_message' => 'New report created'
			]);

		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($report_id,$action='view')
	{
		$user = \Auth::user();
        if($user->locations->count()){            
            $location = $user->locations->first();           
        }else{

            return view('no_permission',compact('user'));
        }


		$report = Report::findOrFail($report_id);


		if($report->location->id != $location->id){
			return 'You are trying to access another location\'s report, which is not allowed';
		}

		$data['cash_gross_total'] = 0;
		$data['cash_adj_total'] = 0;		

		$data['cc_gross_total'] = 0;
		$data['cc_adj_total'] = 0;

		$data['total_coins'] = 0;

		foreach($report->machine_readings as $machine_reading){                       
	        
	        	
        	$data['cash_gross_total'] += $machine_reading->meter_1_current - $machine_reading->meter_1_previous;
        	$data['cash_adj_total'] += $machine_reading->meter_1_adj;

        	$data['cc_gross_total'] += $machine_reading->meter_2_current - $machine_reading->meter_2_previous;
        	$data['cc_adj_total'] += $machine_reading->meter_2_adj; 

        	for($i=1;$i<=$machine_reading->machine()->coins;$i++){
        		$data['total_coins'] += $machine_reading->{'meter_'.($i+2).'_current'} - $machine_reading->{'meter_'.($i+2).'_previous'} + $machine_reading->{'meter_'.($i+2).'_adj'};
        	}
	             
	        
	    }

	    $data['cash_sub_total'] = $data['cash_gross_total'] + $data['cash_adj_total'];
	    $data['cc_sub_total'] = $data['cc_gross_total'] + $data['cc_adj_total'];

	    $data['total_sales'] = $data['cash_sub_total'] + $data['cc_sub_total'] ;

	    $data['cash_percent'] = round($data['cash_sub_total'] / $data['total_sales'] * 100);
	    $data['cc_percent'] = round($data['cc_sub_total'] / $data['total_sales'] * 100);

	    $data['per_coin'] = $data['total_sales'] / $data['total_coins'];
			
		
	    $pdf = PDF::loadView('admin.reports.pdf', compact('report','data'));
	    
	    $file_name = str_slug($report->location->name,'_').'_'.$report->type.'_report_'.$report->id.'.pdf';
	    
	    if($action=='download'){
	    	return $pdf->download($file_name);		
	    }
	    
	    return $pdf->stream($file_name);	
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
