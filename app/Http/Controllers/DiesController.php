<?php namespace App\Http\Controllers;

use App\Dies;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DiesController extends Controller {

	
	public function index()
    {
        $user = \Auth::user();
        if($user->locations->count()){
            $location = $user->locations->first();
            $dies = $location->dies;
            
        	return view('dies.index',compact('dies','location'));
        }else{

            return view('no_permission',compact('user'));
        }


    }

	

}
