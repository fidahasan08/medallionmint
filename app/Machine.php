<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Coin;
use App\MachineSettings;

class Machine extends Model {

	protected $fillable = ['location_id','name', 'photo', 'specific_location',
		'asset_num_1','asset_num_2','date_shipped','date_installed',

		'machine_types','machine_colors','machine_keys','prices','motors','button_lights',

		'coins','coin_1','coin_2','coin_3','coin_4',

		'gizmo_models','gizmo_serial','meter_box_models','meter_box_serial','gizmo_fws','gizmo_pic',

		'has_cc','cc_companies','cc_antennas','cc_modem_models','cc_modem_serial','cc_reader_models','cc_reader_serial',

		'bill_validator_models','vault_models','vault_keys','bill_validator_serial','vault_serial',

		'hopper_models_1','hopper_models_2','hopper_models_3','hopper_models_4',
		'hopper_serial_1','hopper_serial_2','hopper_serial_3','hopper_serial_4',

		'current_meter_1','current_meter_2','current_meter_3','current_meter_4','current_meter_5','current_meter_6',
		'starting_meter_1','starting_meter_2','starting_meter_3','starting_meter_4','starting_meter_5','starting_meter_6',

		'shipped_meter_1','shipped_meter_2','shipped_meter_3','shipped_meter_4','shipped_meter_5','shipped_meter_6',
		'shipped_coin_1_id','shipped_coin_2_id','shipped_coin_3_id','shipped_coin_4_id',
		'shipped_coin_1_quantity','shipped_coin_2_quantity','shipped_coin_3_quantity','shipped_coin_4_quantity'

	];

	public function location(){

		return $this->belongsTo('App\Location');
	}

	public function price(){
		$data = json_decode(MachineSettings::where('id',$this->prices)->first()->data);		
		return $data->prices;
	}

	public function price_min(){
		$data = json_decode(MachineSettings::where('id',$this->prices)->first()->data);
		return round($data->level_2_credit/$data->level_2_coin,2);		
	}

	public function price_max(){		
		
		$data = json_decode(MachineSettings::where('id',$this->prices)->first()->data);		
		return round($data->level_1_credit/$data->level_1_coin,2);
	}

	public function coin_1(){
		return Coin::where('id','=',$this->coin_1)->first();
	}

	public function coin_2(){
		return Coin::where('id','=',$this->coin_2)->first();
	}

	public function coin_3(){
		return Coin::where('id','=',$this->coin_3)->first();
	}

	public function coin_4(){
		return Coin::where('id','=',$this->coin_4)->first();
	}


	

}
