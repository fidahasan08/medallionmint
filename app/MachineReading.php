<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineReading extends Model {

	public function machine(){

        return Machine::where('id',$this->machine_id)->first();
    }

}
