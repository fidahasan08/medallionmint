<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MachineReading;

class Report extends Model {

	protected $fillable= ['location_id','prev_report','type','year','month','date','time','read_by','verified_by'];

	public function location(){

		return $this->belongsTo('App\Location');
	}

	public function machine_readings(){

		return $this->hasMany('App\MachineReading');
	}

	


}
