<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Dashboard</title>

    <link href="/lib/bootstrap-3.3.2-dist/css/bootstrap.css" rel="stylesheet"> <!-- changed this from min to try to get modal working, didn't get it working-->
    <link href="/lib/bootstrap-3.3.2-dist/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/lib/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">
    <link href="/lib/bootstrap-clockpicker/bootstrap-clockpicker.css" rel="stylesheet">
    <link href="/lib/alertify/alertify.min.css" rel="stylesheet">
    <link href="/lib/alertify/alertify.default.css" rel="stylesheet">
    <!--
    <link href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.css" rel="stylesheet">
    <link href="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
    -->

    <link href="/lib/chosen_v1.4.0/chosen.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/lib/jquery-1.11.2.min.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/lib/jquery-2.1.1.min.js"></script>
    <script src="/lib/bootstrap-3.3.2-dist/js/bootstrap.js"></script> <!-- changed this from min to try to get modal working, didn't get it working-->
    <script src="/lib/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="/lib/bootstrap-clockpicker/bootstrap-clockpicker.js"></script>
    <script src="/js/bootbox-4.4.0.min.js"></script>
    <script src="/lib/alertify/alertify.min.js"></script>
    <script src="/js/holder.js"></script>
    <!--
    <script src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.js"></script>
    <script src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    -->

    <script src="/lib/geocomplete/jquery.geocomplete.min.js"></script>
      <script src="/lib/chosen_v1.4.0/chosen.jquery.min.js"></script>
    <script src="/js/custom.js"></script>

    <script src="/lib/Highcharts/js/highcharts.js"></script>




  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="/images/logo_small.gif" alt="my.medallionmint.com"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          
          <ul class="nav navbar-nav navbar-right">
            <li><a>{{Auth::user()->name}}</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          
          <h3 class="nav navbar-nav navbar-left top_location_name">{{$location->name}}</h3>
            


         
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">         
          
          <ul class="nav nav-sidebar">
            <li {!! (Request::is('dashboard') ? 'class="active"' : '' ) !!}><a href="dashboard"><i class="glyphicon glyphicon-dashboard"></i>  Dashboard</a></li>
            
            
              <li {!! (Request::is('reports') ? 'class="active"' : '' ) !!} >
                <a href="{{url('reports')}}"><i class="glyphicon glyphicon-book"></i> Reports </a>
              </li>  

              <li {!! (Request::is('machines') ? 'class="active"' : '' ) !!} >
                <a href="{{url('machines')}}"><i class="glyphicon glyphicon-film"></i> 
                  Machines <span class="badge pull-right">{{$location->machine->count()}}</span></a>
              </li>         

             

              <li {!! (Request::is('coins') ? 'class="active"' : '' ) !!} >
                <a href="{{url('coins')}}"><i class="glyphicon glyphicon-copyright-mark"></i> 
                  Coins <span class="badge pull-right">{{$location->coins->count()}}</span>
                </a>
              </li>


              <li {!! (Request::is('dies') ? 'class="active"' : '' ) !!} >
              <a href="{{url('dies')}}"><i class="glyphicon glyphicon-record"></i> 
                Dies <span class="badge pull-right">{{$location->dies->count()}}</span>
              </a>
              </li>


            <li><a href="/auth/logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
          </ul>
         
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          
          @yield('main')      
          
          
         
        </div>
      </div>
    </div>


  </body>
</html>

@if(Session::has('flash_message'))
    <?php echo "<script>alertify.success('". session('flash_message'). "');</script>"; ?>
@endif

<script>
    //$('.table_sortable').DataTable();
</script>

@yield('script')