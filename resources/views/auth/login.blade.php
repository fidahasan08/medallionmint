<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Login</title>

    <link href="/lib/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/login.css" rel="stylesheet">       

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
  </head>

  <body>

	<div class="login_form_wrapper">
		<img class="logo" src="/images/logo_small.gif" alt="logo">			

		
		

		{!! Form::open(array('url'=>'/auth/login','class'=>'login_form')) !!}
			
			<div class="form-group">
				<i class="header_icon glyphicon glyphicon-user"></i>
			</div>

			<div class="form-group">
				<h4>Manager Login</h4>
			</div>
			
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					
					@foreach ($errors->all() as $error)
						<p>{{ $error }}</p>
					@endforeach
					
				</div>
			@endif

				

			<div class="form-group">
					  
				<div class="input-group input-group-md">
					<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
					<div class="icon-addon addon-md">
						<input class="form-control" placeholder="Email Address" name="email" type="text" value="{{ old('email') }}">                 
					</div>
					
				</div>
			</div>

			<div class="form-group">
					  
				<div class="input-group input-group-md">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<div class="icon-addon addon-md">
						<input class="form-control" placeholder="Password" name="password" type="password" value="">                 
					</div>
					
				</div>
			</div>	
			
			<!--
			<div class="form-group">
				
				<div class="checkbox">
					<label>
						<input type="checkbox" name="remember"> Remember Me
					</label>
				</div>
				
			</div>
			-->

			<div class="form-group clearfix">
				
				<input class=" btn btn-primary" type="submit" value="Log In">
				
			</div>

			<!--
			<div class="form-group">
				<a href="/password/email">Forgot Your Password?</a>
			</div>
			-->

		</form>
	</div>
			
	
  </body>
</html>
