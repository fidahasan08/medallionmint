<html>
<head>
	<title>Test Report Title</title>
	<style type="text/css">		
		body{font-size:13px;}
		table.bordered{border-collapse:collapse;}
		table.bordered td, table.bordered th{border:1px solid #ddd;padding:3px 5px;text-align: left;}
	</style>
</head>
<body>
	

	<table width="100%" style="padding-bottom:15px;margin-bottom:30px;border-bottom:1px solid #ddd;">
		<tr>
			<td valign="top" width="450px;">				
				<table>
					<tr>
						<td valign="middle" style="padding-right:15px;"><img src="images/logo_globe.jpg"></td>
						<td valign="top">
							<h2 style="margin:0;">Medallion Mint LLC</h2>
							1201 S. 3rd St.<br>
							Chickasha, OK 73018<br>
							<a style="text-decoration:none;" href="mailto:sales@medallionmint.com">sales@medallionmint.com</a><br>
							phn: 405-222-1133<br>
							fax: 405-224-1970							
						</td>
					</tr>
				</table>
				
			</td>

			<td valign="top">
				<div style="padding-left:5px;">
					<h2 style="margin:0;">{{$report->location->name}}</h2>
					<p style="margin:0;">{!! nl2br(e($report->location->address))!!}</p>
				</div>
				
				<div style="background:#eee;padding:5px;margin-top:5px;">
					<h3 style="color:#6ead43;margin:0;padding-bottom:5px;">{{studly_case($report->type)}} Report of {{$report->month}} {{$report->year}}</h3>
			    	Read By <strong>{{$report->read_by}}</strong> & 
			    	Verified By <strong>{{$report->verified_by}}</strong><br>
			    	Submitted At {{date('d F Y, h:i A',strtotime($report->created_at))}}
				</div>
			
			</td>
		</tr>

	</table>

	

	@foreach($report->machine_readings as $machine_reading)                        
        @if($machine_reading->machine())
        
            <h3 style="margin:0;padding:5px;background:#eee;">{{$machine_reading->machine()->name}} <span style="color:#666;font-weight:normal;">( {{$machine_reading->machine()->price()}} )</span></h3>

            <table width="100%" class="bordered" style="margin-bottom:10px;">                                
                <tr><th>Meter</th><th>Previous</th><th>Current</th><th>Adj</th><th>Total</th><th>Display</th><th>Stock</th></tr>
                <tr><td>Meter 1 (Cash)</td><td>{{$machine_reading->meter_1_previous}}</td><td>{{$machine_reading->meter_1_current}}</td><td>{{$machine_reading->meter_1_adj}}</td><td>${{$machine_reading->meter_1_current-$machine_reading->meter_1_previous+$machine_reading->meter_1_adj}}</td><td></td><td></td></tr>
                <tr><td>Meter 2 (Credit Card)</td><td>{{$machine_reading->meter_2_previous}}</td><td>{{$machine_reading->meter_2_current}}</td><td>{{$machine_reading->meter_2_adj}}</td><td>${{$machine_reading->meter_2_current-$machine_reading->meter_2_previous+$machine_reading->meter_2_adj}}</td><td></td><td></td></tr>
                @for($i=1;$i<=$machine_reading->machine()->coins;$i++)
                <tr>
                    <td style="width:200px;">Meter {{$i+2}} ({{$machine_reading->machine()->{'coin_'.$i}()->name }})</td>
                    <td>{{$machine_reading->{'meter_'.($i+2).'_previous'} }}</td>
                    <td>{{$machine_reading->{'meter_'.($i+2).'_current'} }}</td>
                    <td>{{$machine_reading->{'meter_'.($i+2).'_adj'} }}</td>
                    <td>{{$machine_reading->{'meter_'.($i+2).'_current'} - $machine_reading->{'meter_'.($i+2).'_previous'} + $machine_reading->{'meter_'.($i+2).'_adj'} }}</td>                    
                    <td>{{$machine_reading->{'meter_'.($i+2).'_display'} }}</td>
                    <td>{{$machine_reading->{'meter_'.($i+2).'_stock'} }}</td>
                </tr>
                @endfor
                
            </table>
             
        @endif
    @endforeach

    <table width="100%" style="margin-top:30px;font-size:14px;">
    	<tr>
    		<td width="30%" align="left" valign="top">
    			<table width="100%" class="bordered" style="">
    				<tr><td>Total Coins Sold</td><td>{{$data['total_coins']}}</td></tr>
    				<tr><td>Average Per Coin</td><td>${{round($data['per_coin'],2)}}</td></tr>
    			</table>
    		</td>
    		<td></td>
    		<td width="40%" align="right">
    			<table width="100%" class="bordered" style="border:1px solid #333 !important;">
			    	<tr style="font-size:11px;"><td></td><td>Gross Total</td><td>Adj</td><td>Sub Total</td></tr>
			    	<tr><th>Cash</th><td>${{$data['cash_gross_total']}}</td><td>${{$data['cash_adj_total']}}</td><td>${{$data['cash_sub_total']}} <span style="font-size:11px;color:#666;">({{$data['cash_percent']}}%)</span></td></tr>
			    	<tr><th>Card</th><td>${{$data['cc_gross_total']}}</td><td>${{$data['cc_adj_total']}}</td><td>${{$data['cc_sub_total']}} <span style="font-size:11px;color:#666;">({{$data['cc_percent']}}%)</span></td></tr>
			    	<tr><th style="text-align:right" colspan="3">Total Sales</th><th>${{$data['total_sales']}}</th></tr>
			    </table>
    		</td>
    	</tr>    
    </table>
	
</body>
</html>