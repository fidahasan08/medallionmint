@extends('admin.layout')

@section('main')
   <h1 class="page-header">Add New Location</h1>  

   
   
	{!! Form::open(['url'=>'admin/locations','files'=>'true','class'=>'row']) !!}
   		@if(!count($companies))
	   		<div class="col-sm-4">
		   		<p class="alert alert-warning">
		   			No company found! Please 
		   			<a href="{{ url('admin/companies/create')}}">create a company</a> 
		   			first , then you can add location

		   		</p>
		   	</div>
	   	@else

   			@include('admin.locations.form',['submit_btn_text'=>'Add New Location'])
   		
   		@endif
			
	{!!Form::close()!!}
	

	@include('admin.locations.script')
@stop