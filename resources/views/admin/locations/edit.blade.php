@extends('admin.layout')

@section('main')
   <h1 class="page-header">Edit {!! $location->name !!}</h1>  

   
	{!! Form::model($location,['method'=>'PATCH','url' => ['admin/locations',$location->id ],'files'=>'true', 'class'=>'row edit']) !!}

   		@include('admin.locations.form',['submit_btn_text'=>'Update Location Info'])

	{!!Form::close()!!}

    @include('admin.locations.script')
   
@stop