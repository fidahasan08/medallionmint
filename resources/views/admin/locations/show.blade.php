@extends('admin.layout')

@section('main')
    
    <h1 class="page-header">
        {{ $location->name }}  
        <a href="{{ URL::route('admin.locations.edit',$location->id) }}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
        
    </h1>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab">Reports ({{$location->report->count()}})</a></li>
        <li role="presentation"><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users ({{$location->managers->count()}})</a></li>
        <li role="presentation"><a href="#coins" aria-controls="coins" role="tab" data-toggle="tab">Coins ({{$location->coins->count()}})</a></li>
        <li role="presentation"><a href="#machines" aria-controls="machines" role="tab" data-toggle="tab">Machines ({{$location->machine->count()}})</a></li>
        <li role="presentation"><a href="#dies" aria-controls="dies" role="tab" data-toggle="tab">Dies ({{$location->dies->count()}})</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="reports">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title clearfix"><i class="glyphicon glyphicon-book"></i> Reports (<span class="report_counter">{{$location->report->count()}}</span>)
                        @if($location->machine->count())
                            <button type="button" class="btn btn-xs btn-info pull-right" data-toggle="modal" data-target="#report_modal"> <i class="glyphicon glyphicon-plus"></i> New Report </button>
                        @endif
                    </h3>
                </div>
            

            
                @if(!$location->report->count())
                
                    <p class="alert alert-warning">No report submitted yet</p>
                    
                @else
                <?php $sl = 1; ?>
                <table class="table table-striped table-bordered reports first-last-auto">
                    <tr><th>SL</th><th>Type</th><th>Month</th><th>Year</th><th>Read Date</th><th>Read Time</th>
                        <th>Read By</th><th>Verified By</th><th>Created At</th><th>Action</th></tr>
                    @foreach($location->report()->orderBy('created_at','desc')->get() as $report)
                        <tr>
                            <td>{{$sl++}}</td>                   
                            <td>{{studly_case($report->type)}}</td>
                            <td>{{$report->month}}</td>
                            <td>{{$report->year}}</td>
                            <td>{{date('d F Y',strtotime($report->date))}}</td>
                            <td>{{date('h:i A',strtotime($report->time))}}</td>
                            <td>{{$report->read_by}}</td>
                            <td>{{$report->verified_by}}</td>
                            <td>{{$report->created_at->diffForHumans()}}</td> 
                            <td>
                                <a href="{{URL::route('admin.locations.reports.show',[$location->id,$report->id])}}" target="blank" class="btn btn-xs btn-default" title="view PDF at new tab"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                <a href="{{URL::route('admin.locations.reports.show',[$location->id,$report->id])}}/download" class="btn btn-xs btn-default" title="Download PDF"><i class="glyphicon glyphicon-download"></i> Download</a>
                                <a href="{{URL::route('admin.locations.reports.edit',[$location->id,$report->id])}}" class="btn btn-xs btn-default" ><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                <a href="#" class="admin_report_del_btn btn btn-xs btn-default" data-url="{{url('admin/locations/'.$location->id.'/reports/'.$report->id)}}" data-token="{{csrf_token()}}" ><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                <a href="#" class="btn btn-xs btn-default btn-expand pull-right" title="Expand/Collapse"><i class="glyphicon glyphicon-chevron-down"></i></a>
                            </td>                 
                        </tr>

                        <tr class="expandable">
                            <td colspan="10">
                                @foreach($report->machine_readings as $machine_reading)                        
                                    @if($machine_reading->machine())
                                    <div class="panel panel-info">        
                                        <div class="panel-heading">
                                            <h3 class="panel-title clearfix">{{$machine_reading->machine()->name}} <span class="pull-right">Machine ID # {{$machine_reading->machine_id}}</span></h3>
                                        </div>
                                        <table class="table table-striped table-condensed">                                
                                            <tr><th>Meter</th><th>Previous</th><th>Current</th><th>Adj</th><th>Total</th><th>Display</th><th>Stock</th></tr>
                                            <tr><td><i class="glyphicon glyphicon-usd"></i> Meter 1 (Cash)</td><td>{{$machine_reading->meter_1_previous}}</td><td>{{$machine_reading->meter_1_current}}</td><td>{{$machine_reading->meter_1_adj}}</td><td>{{$machine_reading->meter_1_current-$machine_reading->meter_1_previous+$machine_reading->meter_1_adj}}</td><td></td><td></td></tr>
                                            <tr><td><i class="glyphicon glyphicon-credit-card"></i> Meter 2 (Credit Card)</td><td>{{$machine_reading->meter_2_previous}}</td><td>{{$machine_reading->meter_2_current}}</td><td>{{$machine_reading->meter_2_adj}}</td><td>{{$machine_reading->meter_2_current-$machine_reading->meter_2_previous+$machine_reading->meter_2_adj}}</td><td></td><td></td></tr>
                                            @for($i=1;$i<=$machine_reading->machine()->coins;$i++)
                                            <tr>
                                                <td><i class="glyphicon glyphicon-record"></i> Meter {{$i+2}} ({{$machine_reading->machine()->{'coin_'.$i}()->name }})</td>
                                                <td>{{$machine_reading->{'meter_'.($i+2).'_previous'} }}</td>
                                                <td>{{$machine_reading->{'meter_'.($i+2).'_current'} }}</td>
                                                <td>{{$machine_reading->{'meter_'.($i+2).'_adj'} }}</td>
                                                <td>{{ $machine_reading->{'meter_'.($i+2).'_current'} - $machine_reading->{'meter_'.($i+2).'_previous'} + $machine_reading->{'meter_'.($i+2).'_adj'} }}</td>
                                                <td>{{$machine_reading->{'meter_'.($i+2).'_display'} }}</td>
                                                <td>{{$machine_reading->{'meter_'.($i+2).'_stock'} }}</td>
                                            </tr>
                                            @endfor
                                            
                                        </table>
                                    </div>
                                    @else
                                    <p class="alert alert-danger">Oppps a machine (<strong>machine_id # {{$machine_reading->machine_id}}</strong>) has been deleted after generating report</p>
                                    @endif
                                @endforeach
                            </td>
                        </tr>

                    @endforeach
                </table>
                @endif
            </div>
        </div>



        <div role="tabpanel" class="tab-pane" id="users">
            <div class="panel panel-default">
                @if(!$location->managers->count())                
                    <p class="alert alert-warning">
                        No location manager found! 
                    </p>                    
                @else
            
                <?php $sl = 1; ?>
                <table class="table table-striped table-bordered first-last-auto"> 
                    <tr><th>SL</th><th>Name</th><th>Email</th><th>Action</th></tr>
                    
                    @foreach($location->managers as $user)
                        <tr>
                            <td>{{$sl++}}</td><td>{{$user->name}}</td><td>{{$user->email}}</td>
                            <td>
                                <a href="{{ URL::route('admin.users.edit',$user->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                                <a title="Remove Access" data-url="{{url('admin/users/'.$user->id.'/remove/'.$location->id)}}" data-token="{{csrf_token()}}" class="admin_location_manager_remove_btn btn btn-xs btn-warning" href="#"><i class="glyphicon glyphicon-remove"></i> Remove Access</a>&nbsp;&nbsp;
                                @if($user->admin)
                                <a class="btn btn-xs btn-danger disabled" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                @else
                                <a title="delete" data-url="{{url('admin/users/'.$user->id)}}" data-token="{{csrf_token()}}" class="admin_user_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                                @endif

                            </td>
                        </tr>
                    @endforeach
                </table>
                @endif       

            
                
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="coins">
            <div class="panel panel-default">
                
                @if(!$location->coins->count())
                
                    <p class="alert alert-warning">
                        No coin found! Please
                        <a href="{{ url('admin/coins/create')}}">add a coin</a>
                    </p>
                    
                @else
            
                <?php $sl = 1; ?>
                <table class="table table-striped table-bordered">
                    <tr><th>SL</th><th>Coin Name</th><th>Front Die</th><th>Back Die</th><th>Action</th></tr>
                    @foreach($location->coins as $coin)
                        <tr>
                            <td>{{$sl++}}
                            <td>{{$coin->name}}</td>
                            <td>
                                {!! Html::image('upload/50-'.$coin->front_dies()->photo, $coin->front_dies()->photo, array('class' => 'dies_photo thumb pull-left')) !!}

                                {{$coin->front_dies()->name}}

                                <p title="cabinet drawer row col">{{ $coin->front_dies()->cabinet.' '.$coin->front_dies()->drawer.' '.$coin->front_dies()->row.' '.$coin->front_dies()->col}}</p>
                            </td>
                            <td>
                                {!! Html::image('upload/50-'.$coin->back_dies()->photo, $coin->back_dies()->photo, array('class' => 'dies_photo thumb pull-left')) !!}

                                {{$coin->back_dies()->name}}

                                <p title="cabinet drawer row col">{{ $coin->back_dies()->cabinet.' '.$coin->back_dies()->drawer.' '.$coin->back_dies()->row.' '.$coin->back_dies()->col}}</p>
                            </td>
                            <td>

                                <a href="{{ URL::route('admin.coins.edit',$coin->id) }}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                                <a title="delete_coin" data-url="{{url('admin/coins/'.$coin->id)}}" data-token="{{csrf_token()}}" class="admin_coin_del_btn btn btn-xs btn-default" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                            </td>
                        </tr>

                    @endforeach
                </table>
                @endif
            </div>
        </div>


        <div role="tabpanel" class="tab-pane" id="machines">
            <div class="panel panel-default">
                
                @if(!$location->machine->count())
               
                    <p class="alert alert-warning">
                        No machines found! Please
                        <a href="{{ url('admin/machines/create')}}">add a machine</a>
                    </p>
                    
                @else
                <?php $sl = 1; ?>
                <table class="table table-striped table-bordered">
                    <tr><th>SL</th><th>Machines</th><th>Action</th></tr>
                    @foreach($location->machine as $machine)
                        <tr>
                            <td>{{$sl++}}</td>
                            <td>
                                @if($machine->photo!='')
                                    {!! Html::image('upload/50-'.$machine->photo, $machine->photo, array('class' => 'machine_photo thumb pull-left')) !!}
                                @else
                                    {!! Html::image('images/no_image_50x67.png', null, array('class' => 'location_photo thumb pull-left')) !!}
                                @endif
                                <a href="{{ URL::route('admin.machines.edit',$machine->id) }}">{{ $machine->name }}</a>
                            </td>
                            <td>

                                <a href="{{ URL::route('admin.machines.edit',$machine->id) }}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                                <a title="delete" data-url="{{url('admin/machines/'.$machine->id)}}" data-token="{{csrf_token()}}" class="admin_machine_del_btn btn btn-xs btn-default" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                            </td>
                        </tr>

                    @endforeach
                </table>
                @endif
            </div>
        </div>
        

        <div role="tabpanel" class="tab-pane" id="dies">
            <div class="panel panel-default">
                
                @if(!$location->dies->count())
                
                    <p class="alert alert-warning">
                        No dies found! Please
                        <a href="{{ url('admin/dies/create')}}">add a dies</a>
                    </p>
                    
                @else
                <?php $sl = 1; ?>
                <table class="table table-striped table-bordered">
                    <tr><th>SL</th><th>Dies Name</th><th>Action</th></tr>
                    @foreach($location->dies as $item)
                        <tr>
                            <td>{{$sl++}}
                            <td>

                                {!! Html::image('upload/50-'.$item->photo, $item->photo, array('class' => 'dies_photo thumb pull-left')) !!}

                                <h4 class="name" style="margin-top:3px;">{{$item->name}}</h4>

                                <h5 title="cabinet drawer row col">{{ $item->cabinet.' '.$item->drawer.' '.$item->row.' '.$item->col}}</h5>
                            </td>
                            <td>

                                <a href="{{ URL::route('admin.dies.edit',$item->id) }}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                                <a title="delete_die" data-url="{{url('admin/dies/'.$item->id)}}" data-token="{{csrf_token()}}" class="admin_dies_del_btn btn btn-xs btn-default" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                            </td>
                        </tr>

                    @endforeach
                </table>
                @endif
            </div>
        </div>

    </div> <!-- /.tab-content -->


    <div class="modal fade" id="report_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-book"></i> New Report</h4>
                </div>
                <div class="modal-body">
                    

                    @if($last_report_created_ago>25)
                        <p class="text-warning">Daily & Weekly report is disabled, coz last report submitted more than 25 days ago.</p>
                        
                        <a href="{{URL::route('admin.locations.reports.create',$location->id)}}/daily" class="btn btn-sm btn-default disabled"><i class="glyphicon glyphicon-time"></i> Daily Report</a>
                        <a href="{{URL::route('admin.locations.reports.create',$location->id)}}/weekly" class="btn btn-sm btn-default disabled"><i class="glyphicon glyphicon-th"></i> Weekly Report</a>
                    @else
                        <a href="{{URL::route('admin.locations.reports.create',$location->id)}}/daily" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-time"></i> Daily Report</a>
                        <a href="{{URL::route('admin.locations.reports.create',$location->id)}}/weekly" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-th"></i> Weekly Report</a>
                    @endif

                    <a href="{{URL::route('admin.locations.reports.create',$location->id)}}/monthly" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-calendar"></i> Monthly Report</a>
                    

                    <br><br>
                    @if($location->report->count())
                        <p class="text-muted">
                            <i class="glyphicon glyphicon-info-sign"></i> Your last report submitted 
                            <strong>{{$location->report()->orderBy('created_at','desc')->first()->created_at->diffForHumans()}}</strong>
                            ({{$location->report()->orderBy('created_at','desc')->first()->type}})
                        </p>
                    @else
                        <p class="text-info"><i class="glyphicon glyphicon-info-sign"></i> You have not submitted any report yet</p>
                    @endif

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>            
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    @include('admin.locations.script')
@stop