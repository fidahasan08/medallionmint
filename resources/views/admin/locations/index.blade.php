@extends('admin.layout')

@section('main')
   <h1 class="page-header">Locations</h1>
   @if(!count($locations))
       <div class="col-sm-4">
           <p class="alert alert-warning">
               No locations found! Please
               <a href="{{ url('admin/locations/create')}}">create a location</a>
           </p>
       </div>
   @else
   	<?php $sl = 1; ?>
   	<table class="table table-striped table-bordered table_sortable">
        <thead>
            <tr><th>SL</th><th>Locations</th><th>Machines</th><th>Company</th><th>Action</th></tr>
        </thead>
	   	@foreach($locations as $location)
	   	<tr>
	   		<td>{{$sl++}}</td>
	   		<td>
                @if($location->photo!='')
                    {!! Html::image('upload/75-'.$location->photo, $location->photo, array('class' => 'location_photo thumb pull-left')) !!}
                @else
                    {!! Html::image('images/no_image_75x50.png', null, array('class' => 'location_photo thumb pull-left')) !!}
                @endif

	   			<a href="{{ URL::route('admin.locations.show',$location->id) }}"><strong>{{$location->name}}</strong></a>
          <p>{!! nl2br(e($location->address))!!}</p>
	   			
	   		</td>
        
        <td>{{$location->machine->count()}}</td>

	   		<td>
	   			<a href="{{ URL::route('admin.companies.show',$location->company->id) }}">{{ $location->company->name }}</a>
	   		</td>   			   		
	   		<td>
	   			
	   			<a href="{{ URL::route('admin.locations.edit',$location->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
				<a title="delete" data-url="{{url('admin/locations/'.$location->id)}}" data-token="{{csrf_token()}}" class="admin_location_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a> 

	   		</td>
	   	</tr>
	   	@endforeach
   	</table>

   @endif


	@include('admin.locations.script')
@stop
