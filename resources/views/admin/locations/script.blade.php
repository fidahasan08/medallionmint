<script>
    $(document).ready(function(){

    /**
     * Show/hide corporate shipper no depending on carrier selected
     */
        if($('#shipping_method').val() == 'USPS' || $('#shipping_method').val() == ''){
            $('.corporate_shipper_no').hide();
        }else{
            $('.corporate_shipper_no').show();
        }


        $('#shipping_method').change(function(){

            if($(this).val() == 'USPS' || $('#shipping_method').val() == ''){
                $('.corporate_shipper_no').slideUp();
            }else{
                $('.corporate_shipper_no').slideDown();
            }
        });


        /**
         * delete locations button
         */
        $('.admin_location_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this location? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Location',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Location has been deleted');
                                $('.nav-sidebar .badge.location').html(parseInt($('.nav-sidebar .badge.location').html())-1);

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete location. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        /**
         * delete machine
         */

        $('.admin_machine_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');




            alertify.confirm('Are you sure you want to delete this machine? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Machine',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().slideUp();
                                });

                                alertify.success('Machine has been deleted');
                                $('.nav-sidebar .badge.machine').html(parseInt($('.nav-sidebar .badge.machine').html())-1);

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete machine. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        /**
         * delete coins
         */
        $('.admin_coin_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this Coin ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Coin',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $('.nav-sidebar .badge.coins').html(parseInt($('.nav-sidebar .badge.coins').html())-1);

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Coin has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete Coin. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        /**
         * delete die
         */

        $('.admin_dies_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this Dies ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Dies',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $('.nav-sidebar .badge.dies').html(parseInt($('.nav-sidebar .badge.dies').html())-1);

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Dies has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete dies. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });
        
        $('.admin_report_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('<h5 class="text-danger"><i class="glyphicon glyphicon-warning-sign"></i> You will lose data permanently.</h5>'+
                    'Are you sure you want to delete this report ? This action cannot be undone.').setting({

                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Report',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).closest('tr').css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $('.panel-heading .report_counter').html(parseInt($('.panel-heading .report_counter').html())-1);

                                $(target).closest('tr').fadeOut(500,function(){
                                    $(target).closest('tr').next('tr.expandable').remove();
                                    $(target).closest('tr').remove();
                                });

                                alertify.success('Report has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).closest('tr').css('opacity','1');
                                alertify.error('Failed to delete report. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });
        

        $('.admin_user_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this User ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete User',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $('.nav-sidebar .badge.users').html(parseInt($('.nav-sidebar .badge.users').html())-1);

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('User has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete user. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        $('.admin_location_manager_remove_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('This user can not be able to access this location anymore.').setting({
                'title':'<span class="glyphicon glyphicon-remove"></span> Remove Location Manager',
                'labels':{ok:'REMOVE ACCESS'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            console.log(res);

                            if(res=='success'){

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Removed Location Manager');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to remove user. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        /*Reports Table of location view page*/
        $('.table.reports .btn-expand').click(function(e){
            e.preventDefault();

           
            $(this).closest('tr').toggleClass('highlight').next().toggle(); 

            $(this).toggleClass('btn-default btn-warning').find('.glyphicon').toggleClass('glyphicon-chevron-up glyphicon-chevron-down');


        });

        // Javascript to enable link to tab
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
        } 

        // Change hash for page-reload
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        })

    });//end document ready
</script>