<div class="col-sm-4">
		
	@include('partial.form_error')

    @if(isset($location) && $location->photo!='')
        <div class="form-group row">
            <div class="col-sm-6">
                {!! Html::image('upload/400-'.$location->photo,$location->photo,array('class'=>'photo')) !!}
            </div>
        </div>
    @endif

	<div class="form-group">				
		{!!Form::label('company_id','Select Company')!!}
        {!!Form::select('company_id',array(null=>'Please Select')+$companies,null,array('class'=>'form-control searchable_select')) !!}
	</div>

	<div class="form-group">				
		{!!Form::label('name','Location Name')!!}
		{!!Form::text('name',null,array('class'=>'form-control')) !!}   
	</div>

	<div class="form-group">				
		{!!Form::label('address','Location Address')!!}
		{!!Form::textarea('address',null,array('class'=>'form-control','rows'=>'3')) !!}   
	</div>

	<div class="form-group">				
		{!!Form::label('website','Website')!!}
		{!!Form::text('website',null,array('class'=>'form-control')) !!}   
	</div>

	<div class="form-group">				
		{!!Form::label('shipping_method','Preferred Shipping Method')!!}
		{!!Form::select('shipping_method',array(
            ''=>'Please Select',
			'USPS'=>'USPS',
			'Fed Ex Ground'=>'Fed Ex Ground',
			'UPS Ground'=>'UPS Ground'
			),null,array('class'=>'form-control')) !!}   
	</div>

	<div class="form-group corporate_shipper_no">				
		{!!Form::label('corporate_shipper_no','Corporate Shipper #')!!}
		{!!Form::text('corporate_shipper_no',null,array('class'=>'form-control')) !!}   
	</div>

	<div class="form-group">				
		{!!Form::label('notes','Notes')!!}
		{!!Form::text('notes',null,array('class'=>'form-control')) !!}   
	</div>

    <div class="form-group">
            {!! Form::label('photo','Upload Location Photo') !!}
            {!! Form::file('photo',array('class'=>'form-control')) !!}
    </div>

	<div class="form-group">
        @if(Request::is('admin/locations/*/edit'))
            <a href="{{ URL::route('admin.locations.show', $location->id) }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
        @elseif(Request::is('admin/locations/create'))
            <a href="{{ URL::route('admin.locations.index') }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
        @endif
		{!! Form::submit($submit_btn_text,['class'=>'btn btn-primary']) !!}
		
	</div>

	
</div>

@include('admin.locations.script')