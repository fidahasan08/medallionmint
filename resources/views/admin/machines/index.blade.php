@extends('admin.layout')

@section('main')
   <h1 class="page-header">Machines</h1>
   @if(!count($machines))
       
     <p class="alert alert-warning">
         No machines found! 
         <a href="{{ url('admin/machines/create')}}">Add new machine</a>
     </p>
       
   @else
   	<?php $sl = 1; ?>
   	<table class="table table-striped table-bordered table_sortable">
        <thead>
            <tr><th>SL</th><th>Machines</th><th>Location</th><th>Company</th><th>Action</th></tr>
        </thead>
	   	@foreach($machines as $machine)
	   	<tr>
	   		<td>{{$sl++}}</td>
        <td>
	        @if($machine->photo!='')
		        {!! Html::image('upload/50-'.$machine->photo, $machine->photo, array('class' => 'machine_photo thumb pull-left')) !!}
	        @else
		        {!! Html::image('images/no_image_75x50.png', null, array('class' => 'machine_photo thumb pull-left')) !!}
	        @endif
          <a href="{{ URL::route('admin.machines.edit',$machine->id) }}">{{ $machine->name }}</a>
        </td>

        <td>          
          <a href="{{ URL::route('admin.locations.show',$machine->location->id) }}">{{ $machine->location->name }}</a>
        </td>


	   		<td>	   		  
          <a href="{{ URL::route('admin.companies.show',$machine->location->company->id) }}">{{ $machine->location->company->name }}</a>
	   		</td>

        

	   		<td>	   			
	   			<a href="{{ URL::route('admin.machines.edit',$machine->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
				  <a title="delete" data-url="{{url('admin/machines/'.$machine->id)}}" data-token="{{csrf_token()}}" class="admin_machine_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a> 
	   		</td>
	   	</tr>
	   	@endforeach
   	</table>

   @endif

   @include('admin.machines.script')
@stop
