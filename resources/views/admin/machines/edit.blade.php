@extends('admin.layout')

@section('main')
    <h1 class="page-header">Edit Machine</h1>

    @include('partial.form_error')

    {!! Form::model($machine,['method'=>'PATCH','url' => ['admin/machines',$machine->id ],'files'=>'true', 'class'=>'machine_form']) !!}
        

        @include('admin.machines.form')
    
        <div class="form-group">
            {!! Form::submit('Update Machine',['class'=>'btn btn-primary']) !!}
        </div>
        

    {!!Form::close()!!}

    @include('admin.machines.machine_setting_add')
    
    @include('admin.machines.script')

@stop