<div class="row form-group">
	<div class="col-sm-2">
        {!!Form::label('shipped_meter_1','Shipped Meter 1',array('class'=>'form-control-static'))!!}
    </div>
    <div class="col-sm-2">
        {!!Form::text('shipped_meter_1',null,array('class'=>'form-control')) !!}
    </div>
</div>

<div class="row form-group">
	<div class="col-sm-2">
        {!!Form::label('shipped_meter_2','Shipped Meter 2',array('class'=>'form-control-static'))!!}
    </div>
    <div class="col-sm-2">
        {!!Form::text('shipped_meter_2',null,array('class'=>'form-control')) !!}
    </div>
</div>

@for($i=1;$i<=4;$i++)
	<div class="row form-group shipped_meter_wrapper shipped_meter_wrapper_{{$i}}">
		<div class="col-sm-2">
	        {!!Form::label('shipped_meter_'.($i+2),'Shipped Meter '.($i+2),array('class'=>'form-control-static'))!!}
	    </div>
	    <div class="col-sm-2">
	        {!!Form::text('shipped_meter_'.($i+2),null,array('class'=>'form-control')) !!}
	    </div>

	    <div class="col-sm-2">
	        {!!Form::label('shipped_coin_'.$i.'_id','Shipped Coin '.$i,array('class'=>'form-control-static'))!!}
	    </div>
	    <div class="col-sm-2">
	        {!!Form::text('shipped_coin_'.$i.'_id',null,array('class'=>'form-control')) !!}
	    </div>

	    <div class="col-sm-2">
	        {!!Form::label('shipped_coin_'.$i.'_quantity','Shipped Coin '.$i.' Quantity',array('class'=>'form-control-static'))!!}
	    </div>
	    <div class="col-sm-2">
	        {!!Form::text('shipped_coin_'.$i.'_quantity',null,array('class'=>'form-control')) !!}
	    </div>
	</div>
@endfor