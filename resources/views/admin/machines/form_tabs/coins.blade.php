<div class="row form-group">
	<div class="col-sm-3">
		{!!Form::label('coins','Select Number of Coins')!!}
		{!!Form::selectRange('coins',1,4,null,array('class'=>'form-control'))!!}
	</div>

	<div class="col-sm-4 ">
		<p class="alert alert-warning location_select_warning">
			<i class="glyphicon glyphicon-warning-sign"></i> 
			Please  <strong>Select Location</strong> at <strong>Details</strong> tab first,
			you can choose coin only from your selected location
		</p>

		<p class="alert alert-warning location_has_coins_warning">
			<i class="glyphicon glyphicon-warning-sign"></i> 
			Your selected location doesn'nt have any coins yet, Please add coins at this location then try again
			<br><a href="#" class="no_coin_refresh_link">Reload (i have added some coins)</a>
		</p>
	</div>
</div>

<div class="row form-group">
	@for($i=1;$i<=4;$i++)
	<div class="col-sm-3 clearfix coin_wrapper coin_wrapper_{{$i}}">
		{!!Form::label('coin_'.$i,'Coin '.$i)!!}
		{!!Form::select('coin_'.$i,array(null=>'Please Select'),null,array('class'=>'coin form-control'))!!}
		
		<!-- <div class="coin_thumb pull-left front"></div>
		<div class="coin_thumb pull-left back"></div> -->
			
	</div>
	@endfor
</div>