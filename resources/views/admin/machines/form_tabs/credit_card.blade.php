<div class="row form-group">
	<div class="col-sm-2">
		{!!Form::label('has_cc','Has Credit Card',array('class'=>'form-control-static'))!!}
	</div>
	<div class="col-sm-3">
		{!!Form::select('has_cc',array('0'=>'No','1'=>'Yes'),null,array('class'=>'form-control'))!!}
	</div>
</div>

<div class="cc_details">

    <div class="row form-group" data-setting="cc_companies">
        
        <div class="col-sm-2">
            {!!Form::label('cc_companies','CC Company',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'cc_companies','name'=>'cc_companies'])             
        </div>
        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>
        
    </div>

    <div class="row form-group" data-setting="cc_antennas">
        
        <div class="col-sm-2">
            {!!Form::label('cc_antennas','CC Antenna',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'cc_antennas','name'=>'cc_antennas'])             
        </div>
        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>
        
    </div>

    <div class="row form-group" data-setting="cc_modem_models">
        
        <div class="col-sm-2">
            {!!Form::label('cc_modem_models','CC Modem Model',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'cc_modem_models','name'=>'cc_modem_models'])             
        </div>
        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>

        <div class="col-sm-2">
            {!!Form::label('cc_modem_serial','CC Modem Serial',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            {!!Form::text('cc_modem_serial',null,array('class'=>'form-control'))!!}          
        </div>   
        
    </div>



    <div class="row form-group" data-setting="cc_reader_models">
        
        <div class="col-sm-2">
            {!!Form::label('cc_reader_models','CC Reader Model',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'cc_reader_models','name'=>'cc_reader_models'])             
        </div>
        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>

        <div class="col-sm-2">
            {!!Form::label('cc_reader_serial','CC Reader Serial',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            {!!Form::text('cc_reader_serial',null,array('class'=>'form-control'))!!}          
        </div>      
        
    </div>

</div> <!-- /.cc_details -->