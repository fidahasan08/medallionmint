@for($i=1;$i<=6;$i++)
    <div class="row form-group current_meter_wrapper current_meter_wrapper_{{$i}}">
        
        <div class="col-sm-2">
            {!!Form::label('current_meter_'.$i,'Current Meter '.$i,array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-2">
            {!!Form::text('current_meter_'.$i,null,array('class'=>'form-control')) !!}
        </div>

        <div class="col-sm-2 col-sm-offset-1">
            {!!Form::label('starting_meter_'.$i,'Starting Meter '.$i,array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-2">
            {!!Form::text('starting_meter_'.$i,null,array('class'=>'form-control')) !!}
        </div>
                
    </div>
@endfor