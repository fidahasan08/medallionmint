<div class="row">
    
    <div class="col-sm-5">

        <div class="row form-group" data-setting="gizmo_models">
            
            <div class="col-sm-4">
                {!!Form::label('gizmo_models','Gizmo Model',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'gizmo_models','name'=>'gizmo_models'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group" data-setting="meter_box_models">
            
            <div class="col-sm-4">
                {!!Form::label('meter_box_models','Meter Box Model',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'meter_box_models','name'=>'meter_box_models'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group" data-setting="gizmo_fws">
            
            <div class="col-sm-4">
                {!!Form::label('gizmo_fws','Gizmo FW',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'gizmo_fws','name'=>'gizmo_fws'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group">            
            <div class="col-sm-4">
                {!!Form::label('gizmo_serial','Gizmo Serial',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                {!!Form::text('gizmo_serial',null,array('class'=>'form-control','placeholder'=>'Gizmo Serial')) !!}            
            </div>            
        </div>

        <div class="row form-group">            
            <div class="col-sm-4">
                {!!Form::label('meter_box_serial','Meter Box Serial',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                {!!Form::text('meter_box_serial',null,array('class'=>'form-control','placeholder'=>'Meter Box Serial')) !!}            
            </div>            
        </div>

    </div>    
   

    <div class="col-sm-3">
        <img class="thumbnail" data-src="holder.js/100%x240" alt="">
    </div>
</div>