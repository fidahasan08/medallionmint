@for($i=1;$i<=4;$i++)
    <div class="row form-group hopper_models_wrapper hopper_models_wrapper_{{$i}}" data-setting="hopper_models">
        
        <div class="col-sm-2">
            {!!Form::label('hopper_models_'.$i,'Hopper Model '.$i,array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'hopper_models','name'=>'hopper_models_'.$i])     
        </div>

        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>

        <div class="col-sm-2">
            {!!Form::label('hopper_serial_'.$i,'Hopper Serial '.$i,array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-2">
            {!!Form::text('hopper_serial_'.$i,null,array('class'=>'form-control')) !!}
        </div>
                
    </div>
@endfor