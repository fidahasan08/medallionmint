<div class="row">
    
    <div class="col-sm-5">       

        <div class="row form-group" data-setting="bill_validator_models">
            
            <div class="col-sm-4">
                {!!Form::label('bill_validator_models','Bill Validator Model',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'bill_validator_models','name'=>'bill_validator_models'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group" data-setting="vault_models">
            
            <div class="col-sm-4">
                {!!Form::label('vault_models','Vault Model',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'vault_models','name'=>'vault_models'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group" data-setting="vault_keys">
            
            <div class="col-sm-4">
                {!!Form::label('vault_keys','Vault Key',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'vault_keys','name'=>'vault_keys'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group">
            
            <div class="col-sm-4">
                {!!Form::label('bill_validator_serial','Bill Validator Serial',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                {!!Form::text('bill_validator_serial',null,array('class'=>'form-control'))!!}          
            </div>        
            
        </div>

        <div class="row form-group">
            
            <div class="col-sm-4">
                {!!Form::label('vault_serial','Vault Serial',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                {!!Form::text('vault_serial',null,array('class'=>'form-control'))!!}          
            </div>        
            
        </div>

    </div><!-- col-sm-5 -->
</div> <!-- /row -->