<div class="row">
    <div class="col-sm-2">
	    @if(isset($machine) && $machine->photo!='')
		    {!! Html::image('upload/400-'.$machine->photo,$machine->photo,array('class'=>'photo', 'width' => 217)) !!}
	    @else
            <img class="thumbnail img-responsive" data-src="holder.js/100%x330">
	    @endif
	    {!! Form::file('photo',array('class'=>'form-control')) !!}
    </div>
    <div class="col-sm-5">

        <div class="row form-group @if ($errors->has('location_id')) has-error @endif">
            
            <div class="col-sm-5">
                {!!Form::label('location_id','Select Location',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-7">
                {!!Form::select('location_id',array(null=>'Please Select')+$locations,null,array('class'=>'form-control searchable_select')) !!}
                @if ($errors->has('location_id')) <p class="help-block">{{ $errors->first('location_id') }}</p> @endif
            </div>
            
        </div>

       

        <div class="row form-group @if ($errors->has('name')) has-error @endif">            
            <div class="col-sm-5">
                {!!Form::label('name','Machine Name',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-7">
                {!!Form::text('name',null,array('class'=>'form-control','placeholder'=>'Machine Name')) !!}
                @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
            </div>
            
        </div>

        <div class="row form-group @if ($errors->has('specific_location')) has-error @endif">
            
            <div class="col-sm-5">
                {!!Form::label('specific_location','Specific Location',array('class'=>'form-control-static') )!!}
            </div>
            <div class="col-sm-7">
                {!!Form::text('specific_location',null,array('class'=>'form-control','placeholder'=>'Specific Location')) !!}
                @if ($errors->has('specific_location')) <p class="help-block">{{ $errors->first('specific_location') }}</p> @endif
            </div>
            
        </div>

        <div class="row form-group @if ($errors->has('asset_num_1')) has-error @endif">
            
            <div class="col-sm-5">
                {!!Form::label('asset_num_1','Asset Number 1',array('class'=>'form-control-static')) !!}
            </div>
            <div class="col-sm-7">
                {!!Form::text('asset_num_1',null,array('class'=>'form-control','placeholder'=>'Asset Number 1')) !!}
                @if ($errors->has('asset_num_1')) <p class="help-block">{{ $errors->first('asset_num_1') }}</p> @endif
            </div>
            
        </div>

        <div class="row form-group @if ($errors->has('asset_num_2')) has-error @endif">
            
            <div class="col-sm-5">
                {!!Form::label('asset_num_2','Asset Number 2',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-7">
                {!!Form::text('asset_num_2',null,array('class'=>'form-control','placeholder'=>'Asset Number 2')) !!}
                @if ($errors->has('asset_num_2')) <p class="help-block">{{ $errors->first('asset_num_2') }}</p> @endif
            </div>
            
        </div>

        <div class="row form-group @if ($errors->has('date_shipped')) has-error @endif">
            
            <div class="col-sm-5">
                {!!Form::label('date_shipped','Date Shipped',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-7">
                {!!Form::text('date_shipped', null ,array('class'=>'datepicker form-control','placeholder'=>'Date Shipped')) !!}
                @if ($errors->has('date_shipped')) <p class="help-block">{{ $errors->first('date_shipped') }}</p> @endif

            </div>
            
        </div>

        <div class="row form-group @if ($errors->has('date_installed')) has-error @endif">
            
            <div class="col-sm-5">
                {!!Form::label('date_installed','Date Installed',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-7">
                {!!Form::text('date_installed',null,array('class'=>'datepicker form-control','placeholder'=>'Date Installed')) !!}
                @if ($errors->has('date_installed')) <p class="help-block">{{ $errors->first('date_installed') }}</p> @endif
            </div>
           
        </div>

    </div> 

    <div class="col-sm-5">
        <div class="row form-group" data-setting="machine_types">
            
            <div class="col-sm-4">
                {!!Form::label('machine_types','Machine Type',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'machine_types','name'=>'machine_types'])
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group" data-setting="machine_colors">
            
            <div class="col-sm-4">
                {!!Form::label('machine_colors','Machine Color',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'machine_colors','name'=>'machine_colors'])
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group" data-setting="machine_keys">
            
            <div class="col-sm-4">
                {!!Form::label('machine_keys','Machine Keys',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'machine_keys','name'=>'machine_keys'])            
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>


        <div class="row form-group" data-setting="prices">
            
            <div class="col-sm-4">
                {!!Form::label('prices','Prices',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'prices','name'=>'prices'])                
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>

        <div class="row form-group" data-setting="motors">
            
            <div class="col-sm-4">
                {!!Form::label('motors','Motors',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'motors','name'=>'motors'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>


        <div class="row form-group" data-setting="button_lights">
            
            <div class="col-sm-4">
                {!!Form::label('button_lights','Button Lights',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'button_lights','name'=>'button_lights'])                
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>
    </div> 

</div>