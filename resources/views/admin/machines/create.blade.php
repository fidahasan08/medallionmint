@extends('admin.layout')

@section('main')
    <h1 class="page-header">Add New Machine</h1>

    @include('partial.form_error')

    {!! Form::open(['url'=>'admin/machines','files'=>'true','class'=>'machine_form']) !!}

        @if(!count($locations))            
            <p class="alert alert-warning">
                No Location found! Please <a href="{{ url('admin/locations/create')}}">create a location</a>
                first , then you can add machine
            </p>            
        @else

            @include('admin.machines.form')
        
            <div class="form-group">
                {!! Form::submit('Create New Machine',['class'=>'btn btn-primary']) !!}
            </div>
        @endif

    {!!Form::close()!!}
    
    @include('admin.machines.machine_setting_add')
    @include('admin.machines.script')

@stop