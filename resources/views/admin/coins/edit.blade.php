@extends('admin.layout')

@section('main')
    <h1 class="page-header">Edit Coin - {{ $coin->name }}</h1>

    <div class="row">

        {!! Form::model($coin,['method'=>'PATCH','url' => ['admin/coins',$coin->id ], 'class'=>'col-sm-12 col-lg-6 edit']) !!}

        @include('admin.coins.form',['submit_btn_text'=>'Update Coin Info'])


        {!!Form::close()!!}
    </div>

    @include('admin.coins.script')

@stop