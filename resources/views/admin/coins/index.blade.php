@extends('admin.layout')

@section('main')
    <h1 class="page-header">Coins</h1>

    @if(!count($coins))
        <div class="col-sm-4">
            <p class="alert alert-warning">
                No coins found! Please
                <a href="{{ url('admin/coins/create')}}">create a coin</a>
            </p>
        </div>
    @else
        <?php $sl = 1; ?>
        <table class="table table-striped table-bordered coins">
            <tr><th>SL</th><th>Name</th><th>Front Die</th><th>Back Die</th><th>Location</th><th>Top Inventory</th><th>Inventory</th>
                <th>Metal</th><th>Finish</th><th>Notes</th><th>Action</th></tr>
            @foreach($coins as $coin)
                <tr>
                    <td>{{$sl++}}</td>
                    <td>{{$coin->name}}</td>
                    <td>

                        {!! Html::image('upload/50-'.$coin->front_dies()->photo, $coin->front_dies()->photo, array('class' => 'dies_photo thumb pull-left')) !!}

                        {{$coin->front_dies()->name}}

                        <p title="cabinet drawer row col">{{ $coin->front_dies()->cabinet.' '.$coin->front_dies()->drawer.' '.$coin->front_dies()->row.' '.$coin->front_dies()->col}}</p>
                    </td>

                    <td>

                        {!! Html::image('upload/50-'.$coin->back_dies()->photo, $coin->back_dies()->photo, array('class' => 'dies_photo thumb pull-left')) !!}

                        {{$coin->back_dies()->name}}

                        <p title="cabinet drawer row col">{{ $coin->back_dies()->cabinet.' '.$coin->back_dies()->drawer.' '.$coin->back_dies()->row.' '.$coin->back_dies()->col}}</p>
                    </td>

                    <td>
                        <a href="{{ URL::route('admin.locations.show',$coin->location->id) }}">{{ $coin->location->name }}</a>
                    </td>
                    <td>{{$coin->top_inventory}}</td>
                    <td>{{$coin->inventory}}</td>
                    <td>{{$coin->metal}}</td>
                    <td>{{$coin->finish}}</td>
                    <td>{{$coin->notes}}</td>

                    <td>

                        <a href="{{ URL::route('admin.coins.edit',$coin->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/coins/'.$coin->id)}}" data-token="{{csrf_token()}}" class="admin_coin_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>

                </tr>
            @endforeach
        </table>

    @endif

    @include('admin.coins.script')


@stop