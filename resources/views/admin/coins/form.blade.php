<div class="form-group">

    @include('partial.form_error')

</div>

<div class="row">
    <div class="col-sm-5">
        <div class="form-group">

            {!! Form::label('name','Coin Name') !!}
            {!! Form::text('name',null,array('class'=>'form-control') ) !!}

        </div>

        <div class="form-group">
            {!! Form::label('location_id','Location') !!}
            {!! Form::select('location_id',$locations,null,array('class'=>'form-control searchable_select') ) !!}
        </div>

        <div class="form-group">

            {!! Form::label('top_inventory','Top Inventory') !!}
            {!! Form::text('top_inventory',null,array('class'=>'form-control') ) !!}
        </div>

        <div class="form-group">

            {!! Form::label('inventory','Inventory') !!}
            {!! Form::text('inventory',null,array('class'=>'form-control') ) !!}

        </div>



        <div class="form-group">
            {!! Form::label('notes','Notes') !!}
            {!! Form::textarea('notes',null,array('class'=>'form-control','rows'=>'2') ) !!}
        </div>


    </div>

    <div class="col-sm-7">
        <div class="row">
            <div class="col-sm-6 form-group">

                {!! Form::label('metal','Metal') !!}
                {!! Form::select('metal',array(
                'Brass'=>'Brass',
                'Copper'=>'Copper',
                'Nickel Silver'=>'Nickel Silver'
                ),null,array('class'=>'form-control') ) !!}
            </div>

            <div class="col-sm-6 form-group">
                {!! Form::label('finish','Finish') !!}
                {!! Form::select('finish',array(
                'Shiny'=>'Shiny',
                'Antique'=>'Antique'
                ),null,array('class'=>'form-control') ) !!}
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6 form-group">
                {!! Form::label('front_dies_type','Front Dies Type') !!}
                {!! Form::select('front_dies_type',array(
                'Location Dies'=>'Location Dies',
                'Stock Dies'=>'Stock Dies'
                ),null,array('class'=>'form-control') ) !!}

            </div>

            <div class="col-sm-6 form-group">
                {!! Form::label('back_dies_type','Back Dies Type') !!}
                {!! Form::select('back_dies_type',array(
                'Location Dies'=>'Location Dies',
                'Stock Dies'=>'Stock Dies'
                ),null,array('class'=>'form-control') ) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 form-group">
                {!! Form::label('front_dies','Front Dies') !!}

                @if(isset($coin->front_dies_type))
                    @if($coin->front_dies_type == 'Location Dies')

                        <select name="front_dies" id="front_dies" class="form-control">
                            <option value="">Please Select</option>
                            @foreach($location_dies as $dies)
                                <option
                                        value="{{ $dies->id }}"
                                        data-imagesrc="{{ $dies->photo}}"
                                        {{ ($dies->id == $coin->front_dies ? 'selected':'') }}
                                        >{{$dies->name}}</option>

                            @endforeach
                        </select>
                    @else
                        <select name="front_dies" id="front_dies" class="form-control">
                            <option value="">Please Select</option>
                            @foreach($stock_dies as $dies)
                                <option
                                        value="{{ $dies->id }}"
                                        data-imagesrc="{{ $dies->photo}}"
                                        {{ ($dies->id == $coin->front_dies ? 'selected':'') }}
                                        >{{$dies->name}}</option>

                            @endforeach
                        </select>
                    @endif
                @else
                    {!! Form::select('front_dies',array(),null,array('class'=>'form-control') ) !!}
                @endif



                @if(isset($coin->metal))
                    <div class="dies front {!! strtolower($coin->metal) !!}">
                        <img src="/upload/100-{!! $coin->front_dies()->photo !!}">

                    </div>
                @else
                    <div class="dies front brass">		</div>
                @endif



            </div>

            <div class="col-sm-6 form-group">
                {!! Form::label('back_dies','Back Dies') !!}

                @if(isset($coin->back_dies_type))
                    @if($coin->back_dies_type == 'Location Dies')

                        <select name="back_dies" id="back_dies" class="form-control">
                            <option value="">Please Select</option>
                            @foreach($location_dies as $dies)
                                <option
                                        value="{{ $dies->id }}"
                                        data-imagesrc="{{ $dies->photo}}"
                                        {{ ($dies->id == $coin->back_dies ? 'selected':'') }}
                                        >{{$dies->name}}</option>

                            @endforeach
                        </select>
                    @else
                        <select name="back_dies" id="back_dies" class="form-control">
                            <option value="">Please Select</option>
                            @foreach($stock_dies as $dies)
                                <option
                                        value="{{ $dies->id }}"
                                        data-imagesrc="{{ $dies->photo}}"
                                        {{ ($dies->id == $coin->back_dies ? 'selected':'') }}
                                        >{{$dies->name}}
                                </option>

                            @endforeach
                        </select>
                    @endif
                @else
                    {!! Form::select('back_dies',array(),null,array('class'=>'form-control') ) !!}
                @endif



                @if(isset($coin->metal))
                    <div class="dies back {!! strtolower($coin->metal) !!}">
                        <img src="/upload/100-{!! $coin->back_dies()->photo !!}">

                    </div>
                @else
                    <div class="dies back brass">		</div>
                @endif



            </div>
        </div>

    </div>

</div>

<div class="form-group">
    @if(Request::is('admin/coins/*/edit'))
        <a href="{{ URL::route('admin.coins.show', $coin->id) }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
    @elseif(Request::is('admin/coins/create'))
        <a href="{{ URL::route('admin.coins.index') }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
    @endif
    {!! Form::submit($submit_btn_text,['class'=>'btn btn-primary']) !!}
</div>