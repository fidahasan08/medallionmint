<script type="text/javascript">
	$('document').ready(function(){

		$('.admin_user_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this User ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete User',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $('.nav-sidebar .badge.users').html(parseInt($('.nav-sidebar .badge.users').html())-1);

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('User has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete user. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });
        
        $('.admin_location_manager_remove_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            

            alertify.confirm('This user can not be able to access this location anymore.').setting({
                'title':'<span class="glyphicon glyphicon-remove"></span> Remove Location Manager Access',
                'labels':{ok:'REMOVE ACCESS'},
                'onok': function(){
                    target.parent().remove();
                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_token: token},
                        success: function (res) {
                            //console.log(res);
                            if(res=='success'){                              
                                
                            }else{
                              
                            }
                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });


        $('.manage_location_access_btn').click(function(e){

            e.preventDefault();
            var target = $(this);
            var existing_locations_holder = $('#manage_location_access_modal .modal-body .existing_locations');
            existing_locations_holder.html(target.closest('tr').find('td.locations').html()).find('a.admin_location_manager_remove_btn').remove();

            var user_id = target.closest('tr').data('user-id');

            $('#user_id').val(user_id);
            $('.location_selector').val('');


        });


        $('#manage_location_access_modal').on('shown.bs.modal', function () {          
            $('.location_selector', this).chosen('destroy').chosen();
        });



	});
</script>