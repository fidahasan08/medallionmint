@extends('admin.layout')

@section('main')
    <h1 class="page-header">{{ $company->name }}  <a href="{{ URL::route('admin.companies.edit',$company->id) }}" class="btn btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;&nbsp;</h1>

    <h3 class="page-header">Locations</h3>
    @if(!count($locations))
        <div class="row">
            <div class="col-sm-4">
                <p class="alert alert-warning">
                    No locations found! Please
                    <a href="{{ url('admin/locations/create')}}">add a location</a>
                </p>
            </div>
        </div>
    @else
        <?php $sl = 1; ?>
        <table class="table table-striped table-bordered">
            <tr><th>SL</th><th>Location</th><th>Action</th></tr>
            @foreach($locations as $location)
                <tr>
                    <td>{{$sl++}}
                    <td>
                        @if($location->photo!='')
                            {!! Html::image('upload/75-'.$location->photo, $location->photo, array('class' => 'location_photo thumb pull-left')) !!}
                        @else
                            {!! Html::image('images/no_image_75x50.png', null, array('class' => 'location_photo thumb pull-left')) !!}
                        @endif

                        <a href="{{ URL::route('admin.locations.show',$location->id) }}">{{$location->name}}</a>
                        <p>{{$location->address}}</p>

                    </td>
                    <td>

                        <a href="{{ URL::route('admin.locations.edit',$location->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/locations/'.$location->id)}}" data-token="{{csrf_token()}}" class="admin_location_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>

            @endforeach
        </table>
    @endif

    @include('admin.companies.script')

@stop