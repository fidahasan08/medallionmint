<script>
    $(document).ready(function(){

        /***
         *Delete company button
        * */

        $('.admin_company_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');




            alertify.confirm('Are you sure you want to delete this company? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Company',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Company has been deleted');
                                $('.nav-sidebar .badge.company').html(parseInt($('.nav-sidebar .badge.company').html())-1);

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete company. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        /**
         * delete location from company show
        */
        $('.admin_location_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');




            alertify.confirm('Are you sure you want to delete this location? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Location',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().slideUp();
                                });

                                alertify.success('Location has been deleted');
                                $('.nav-sidebar .badge.location').html(parseInt($('.nav-sidebar .badge.location').html())-1);

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete location. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

       

    });
</script>