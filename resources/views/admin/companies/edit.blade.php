@extends('admin.layout')

@section('main')
   <h1 class="page-header">Edit {!! $company->name !!}</h1>  

   
	{!! Form::model($company,['method'=>'PATCH','url' => ['admin/companies',$company->id ], 'class'=>'row edit']) !!}
   		
   		@include('admin.companies.form',['submit_btn_text'=>'Update Company Info'])
			
	{!!Form::close()!!}
	
@stop