@extends('admin.layout')

@section('main')
   <h1 class="page-header">Add New Company</h1>  

   
	{!! Form::open(['url'=>'admin/companies','class'=>'row']) !!}			
   		
   		@include('admin.companies.form',['submit_btn_text'=>'Add New Company'])
			
	{!!Form::close()!!}

@stop