@extends('admin.layout')

@section('main')
   <h1 class="page-header">Companies</h1>
   @if(!count($companies))
       <div class="col-sm-4">
           <p class="alert alert-warning">
               No companies found! Please
               <a href="{{ url('admin/companies/create')}}">create a company</a>
           </p>
       </div>
   @else
   	<?php $sl = 1; ?>
   	<table class="table table-striped table-bordered table_sortable">
        <thead>
        <tr><th>SL</th><th>Company Name</th><th>Locations</th><th>Machines</th><th>Action</th></tr>
        </thead>
	   	@foreach($companies as $company)
	   	<tr>
	   		<td>{{$sl++}}</td>
	   		<td>
	   			<a href="{{ URL::route('admin.companies.show',$company->id) }}">{{$company->name}}</a>
	   			
	   		</td>	
	   		<td>{{ App\Location::WHERE("company_id","=",$company->id)->WHERE("active","=","1")->count() }}</td>
	   		<td>{{--
                count(App\Company::JOIN("locations", "locations.company_id", "=", "companies.id")
                    ->WHERE("companies.id","=",$company->id)
                    ->JOIN("machines", "machines.location_id", "=", "locations.id")
                    ->WHERE("machines.active","=","1")
                    ->get())
	        
          --}}</td> <!--//TODO ask fida about making this more simple -->
	   		<td>
	   			
	   			<a href="{{ URL::route('admin.companies.edit',$company->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
				<a title="delete" data-url="{{url('admin/companies/'.$company->id)}}" data-token="{{csrf_token()}}" class="admin_company_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a> 

	   		</td>
	   	</tr>
	   	@endforeach
   	</table>

   @endif

    @include('admin.companies.script')
@stop
