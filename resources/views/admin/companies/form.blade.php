<div class="col-sm-4">
		
	@include('partial.form_error')

	
	<div class="form-group">				
		{!!Form::label('name','Company Name')!!}
		{!!Form::text('name',null,array('class'=>'form-control')) !!}   
	</div>

	<div class="form-group">				
		{!!Form::label('address','Company Address')!!}
		{!!Form::textarea('address',null,array('class'=>'form-control','rows'=>'3')) !!}   
	</div>

	<div class="form-group">				
		{!!Form::label('website','Website')!!}
		{!!Form::text('website',null,array('class'=>'form-control')) !!}   
	</div>

	

	<div class="form-group">				
		{!!Form::label('notes','Notes')!!}
		{!!Form::text('notes',null,array('class'=>'form-control')) !!}   
	</div>		

	
	<div class="form-group">
        @if(Request::is('admin/companies/*/edit'))
            <a href="{{ URL::route('admin.companies.show', $company->id) }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
        @elseif(Request::is('admin/companies/create'))
            <a href="{{ URL::route('admin.companies.index') }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
        @endif
		
		{!! Form::submit($submit_btn_text,['class'=>'btn btn-primary']) !!}
		
	</div>

</div>

@include('admin.companies.script')