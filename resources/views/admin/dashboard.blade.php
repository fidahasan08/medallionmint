@extends('admin.layout')

@section('main')
   <h1 class="page-header">Admin Dashboard</h1>

   <div class="row">
       <div class="col-sm-12">
           <div class="row">
               <div class="col-sm-3">@include('admin.charts.top_locations')</div>
               <div class="col-sm-9" id="area_chart_container">@include('admin.charts.area')</div>
           </div>
       </div>
   </div>
   <div class="row">
       <div class="col-sm-12">
           <div class="row">
               <div class="col-sm-5" id="us_map_container">@include('admin.charts.us')</div>
               <div class="col-sm-2" id="shiny_pie_container">@include('admin.charts.shiny_pie')</div>
               <div class="col-sm-2" id="antiqued_pie_container">@include('admin.charts.antiqued_pie')</div>
               <div class="col-sm-3" id="all_coins_pie_container">@include('admin.charts.all_coins_pie')</div>
           </div>
       </div>
   </div>

@stop