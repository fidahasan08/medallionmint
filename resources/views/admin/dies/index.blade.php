@extends('admin.layout')

@section('main')
    <h1 class="page-header">Dies</h1>


    @if(!count($dies))
        <div class="col-sm-4">
            <p class="alert alert-warning">
                No dies found! Please
                <a href="{{ url('admin/dies/create')}}">create a die</a>
            </p>
        </div>
    @else
        <?php $sl = 1; ?>
        <table class="table table-striped table-bordered dies">
            <tr>
                    <th>SL</th><th>Dies</th><th>Location</th><th>Action</th>
            </tr>
            @foreach($dies as $item)
                <tr>
                    <td>{{$sl++}}</td>
                    <td>

                        {!! Html::image('upload/50-'.$item->photo, $item->photo, array('class' => 'dies_photo thumb pull-left')) !!}

                        <h4 class="name">{{$item->name}}</h4>

                        <h5 title="cabinet drawer row col">{{ $item->cabinet.' '.$item->drawer.' '.$item->row.' '.$item->col}}</h5>
                    </td>

                    <td>
                        @if($item->location_id == 0)
                            Stock Die
                        @else
                            <a href="{{ URL::route('admin.locations.show',$item->location->id) }}">{{ $item->location->name }}</a>
                        @endif
                    </td>

                    <td>

                        <a href="{{ URL::route('admin.dies.edit',$item->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/dies/'.$item->id)}}" data-token="{{csrf_token()}}" class="admin_dies_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

    @include('admin.dies.script')

@stop

