@extends('admin.layout')

@section('main')
    <h1 class="page-header">Edit Dies</h1>

    <div class="row">

        {!! Form::model($dies,['method'=>'PATCH','url' => ['admin/dies',$dies->id ],'files'=>'true', 'class'=>'col-sm-12 col-md-6 col-lg-5 edit']) !!}

        @include('admin.dies.form',['submit_btn_text'=>'Update Dies Info'])


        {!!Form::close()!!}
    </div>

@stop