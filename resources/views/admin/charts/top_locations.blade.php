<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">Top Locations</div>
    <!-- List group -->
    <ul class="list-group">
        <li class="list-group-item">Statue Of Liberty <span class="badge">10%</span></li>
        <li class="list-group-item">The Alamo <span class="badge">8%</span></li>
        <li class="list-group-item">Mount Rushmore <span class="badge">7%</span></li>
        <li class="list-group-item">Kyle's House <span class="badge">4%</span></li>
        <li class="list-group-item">The Shop <span class="badge">2%</span></li>
        <li class="list-group-item">Sam Noble Museum <span class="badge">1%</span></li>
    </ul>
</div>