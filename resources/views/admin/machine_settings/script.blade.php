<script type="text/javascript">

	$(document).ready(function(){
		$('.step.machine_types').fadeIn();

		$('.list-group a').click(function(e){
			e.preventDefault();

			$(this).addClass('active').siblings().removeClass('active');
			var target = $(this).data('for');

			
			
			$('.step').fadeOut().promise().done(function(){
				
				$('.step.'+target).fadeIn();
			});
		});


		$('form.step').submit(function(e){          

			e.preventDefault();
            var form = $(this);
            var url = $(this).attr('url');
            var data = $(this).serializeArray();

            $(form)[0].reset();

            
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                beforeSend:function(){

                },
                success:function(res){
                    
                    var new_sl = $(form).find('tr').length - 1;

                    var new_item = '<tr data-id="'+res.id+'"><td>'+new_sl+'</td>';

                    var fields = $.parseJSON(res.data);
                    
                    $.each(fields,function(i){                        
                        new_item += '<td>'+fields[i]+'</td>';
                    });

                    new_item += '<td>'+
                        '<a class="setting_edit_btn btn btn-xs btn-info" href="#"><i class="glyphicon glyphicon-edit"></i> Edit</a> &nbsp; '+
                        '<a class="setting_del_btn btn btn-xs btn-warning" href="#" data-url="{{url('admin/machine_settings/')}}/'+res.id+'" data-token="{{csrf_token()}}"><i class="glyphicon glyphicon-trash"></i> </a>'+

                    '</td></tr>';

                    

                    $(form).find('tbody').append(new_item);

                    alertify.success('New machine setting item added');


                }

            });
            
			
		});

        $(document).on('click','.setting_edit_btn',function(e){        

            e.preventDefault();

            $(this).closest('table').find('.setting_edit_btn').addClass('disabled');

            var row = $(this).closest('tr');          

            var form_row = $(this).closest('table tbody').find('tr:nth-child(1)');
            var cloned = form_row.clone().insertAfter(row).css({'background':'#FBFCD9'});

            form_row.find(':input').not('input[name="setting"]').prop('disabled',true);
            

            row.hide().addClass('updating');

           
            var fields = row.find('td');
            $.each(fields,function(i){
                if(i==0){
                    $(cloned).find('td:nth-child('+(i+1)+')').html($(fields[i]).html());
                }else if( i== (fields.length-1) ){
                    $(cloned).find('td:last-child').html('<button class="setting_update_btn btn btn-xs btn-info">update</button>  &nbsp; <button class="setting_update_cancel_btn btn btn-xs btn-default"><i class="glyphicon glyphicon-remove"></i></button>');
                }else{
                    $(cloned).find('td:nth-child('+(i+1)+') :input').val($(fields[i]).html());
                }
            });

        });

        $(document).on('click','.setting_update_btn',function(e){

            e.preventDefault();
            var row = $(this).closest('table').find('tr.updating');
            var cloned_row = $(this).closest('tr');
            var id = row.data('id');                    
            var url = "{{url('admin/machine_settings')}}"+'/'+id;
            
            var data = $(this).closest('form').serialize()+'&_method=patch';            
            
            
            $.ajax({
                url: url,
                type: 'POST',                                           
                data: data,                                
                success:function(res){                    
                    if(res.status=='success'){
                        alertify.success('Machine Setting updated');

                        var fields = $.parseJSON(res.data);
                        
                        var counter = 2;
                        $.each(fields,function(i){                        
                            console.log(fields[i]);
                            
                            row.find('td:nth-child('+(counter++)+')').html(fields[i]);
                        });                 



                    }else{
                        alertify.error('Error: Failed to update');
                    }


                    row.closest('table').find('.setting_edit_btn').removeClass('disabled');
                    row.closest('table').find(':input').prop('disabled',false);
                    row.closest('table').find('.updating').show().removeClass('updating');
                    cloned_row.remove();

                }

            });       
            

        });

        $(document).on('click','.setting_update_cancel_btn',function(e){

            e.preventDefault();
            $(this).closest('table').find('.setting_edit_btn').removeClass('disabled');
            $(this).closest('table').find(':input').prop('disabled',false);
            $(this).closest('table').find('.updating').show().removeClass('updating');
            $(this).closest('tr').remove();
        });


        $(document).on('click','.setting_del_btn',function(e){        

            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this setting item ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Setting Item',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).closest('tr').css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){                               

                                $(target).closest('tr').fadeOut(500,function(){
                                    $(target).closest('tr').remove();
                                });

                                alertify.success('Setting Item has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).closest('tr').css('opacity','1');
                                alertify.error('Failed to delete setting item. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        
	});

</script>