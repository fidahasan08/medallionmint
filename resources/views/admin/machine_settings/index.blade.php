@extends('admin.layout')

@section('main')
    <h1 class="page-header">Machine Settings</h1>
    @include('partial.form_error')
    
    <div class="row machine_settings">
        <div class="col-md-3">
            <div class="list-group nav">
                <a href="#" class="list-group-item active" data-for="machine_types">1 Machine Types</a>
                <a href="#" class="list-group-item" data-for="machine_colors">2 Machine Colors</a>
                <a href="#" class="list-group-item" data-for="machine_keys">3 Machine Keys</a>
                <a href="#" class="list-group-item" data-for="prices">4 Prices</a>
                <a href="#" class="list-group-item" data-for="motors">5 Motors</a>
                <a href="#" class="list-group-item" data-for="button_lights">6 Button Lights</a>
                <a href="#" class="list-group-item" data-for="gizmo_models">7 Gizmo Models</a>
                <a href="#" class="list-group-item" data-for="meter_box_models">8 Meter Box Models</a>
                <a href="#" class="list-group-item" data-for="gizmo_fws">9 Gizmo Fws</a>
                <a href="#" class="list-group-item" data-for="cc_companies">10 CC Companies</a>
                <a href="#" class="list-group-item" data-for="cc_modem_models">11 CC Modem Models</a>
                <a href="#" class="list-group-item" data-for="cc_reader_models">12 CC Reader Models</a>
                <a href="#" class="list-group-item" data-for="cc_antennas">13 CC Antennas</a>
                <a href="#" class="list-group-item" data-for="bill_validator_models">14 Bill Validator Models</a>
                <a href="#" class="list-group-item" data-for="vault_models">15 Vault Models</a>
                <a href="#" class="list-group-item" data-for="vault_keys">16 Vault Keys</a>
                <a href="#" class="list-group-item" data-for="hopper_models">17 Hopper Models</a>

            </div>
        </div>

        <div class="col-md-9">
            
            <!-- 1 machine_types -->            
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step machine_types']) !!}
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr><th>SL</th><th>Type</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                             
                            <td><input name="type" class="form-control" placeholder="Type" required></td>
                            <td>
                                <input type="hidden" name="setting" value="machine_types">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr> 

                        @include('admin.machine_settings.fetch',['setting'=>'machine_types'])                          
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}


            <!-- 2 machine_colors -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step machine_colors']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr><th>SL</th><th>Type</th><th>SW Code</th><th>Color</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td> 
                            <td>
                                <select name="type" class="form-control" required>
                                    <option value="">Please Select Type</option>
                                    <option value="Clear Coat">Clear Coat</option>
                                    <option value="Paint">Paint</option>
                                    <option value="Stain">Stain</option>
                                </select>
                            </td>
                            <td><input name="sw_code" class="form-control" placeholder="SW Code" required> </td> 
                            <td><input name="color" class="form-control" placeholder="Color" required></td>
                            <td>
                                <input type="hidden" name="setting" value="machine_colors">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'machine_colors'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}


            <!-- 3 machine_keys -->            
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step machine_keys']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr><th>SL</th><th>Machine Keys</th><th></th> </tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="machine_key" class="form-control" placeholder="Machine Key" required></td>
                            <td>
                                <input type="hidden" name="setting" value="machine_keys">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'machine_keys'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 4 prices -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step prices']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Prices</th><th>Level 1 Credit</th><th>Level 1 Coin</th><th>Level 2 Credit</th><th>Level 2 Coin</th><th></th></tr>

                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="prices" class="form-control" placeholder="Prices" required></td>
                            <td><input name="level_1_credit" class="form-control" placeholder="Level 1 Credit" required></td>
                            <td><input name="level_1_coin" class="form-control" placeholder="Level 1 Coin" required></td>
                            <td><input name="level_2_credit" class="form-control" placeholder="Level 2 Credit" required></td>
                            <td><input name="level_2_coin" class="form-control" placeholder="Level 2 Coin" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="prices">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'prices'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 5 motors -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step motors']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Voltage</th><th></th></tr>

                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="voltage" class="form-control" placeholder="Voltage" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="motors">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'motors'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 6 button_lights -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step button_lights']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Type</th><th></th></tr>

                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="type" class="form-control" placeholder="Type" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="button_lights">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'button_lights'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 7 gizmo_models -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step gizmo_models']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Model</th><th></th></tr>

                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="model" class="form-control" placeholder="Model" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="gizmo_models">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'gizmo_models'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 8 meter_box_models -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step meter_box_models']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Model</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="model" class="form-control" placeholder="Model" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="meter_box_models">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'meter_box_models'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 9 gizmo_fws -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step gizmo_fws']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Gizmo FWS</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="gizmo_fws" class="form-control" placeholder="Gizmo FWS" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="gizmo_fws">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'gizmo_fws'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 10 cc_companies -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step cc_companies']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Credit Card Company</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="cc_company" class="form-control" placeholder="Credit Card Company" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="cc_companies">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'cc_companies'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 11 cc_modem_models -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step cc_modem_models']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Model</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="model" class="form-control" placeholder="Model" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="cc_modem_models">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'cc_modem_models'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 12 cc_reader_models -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step cc_reader_models']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Model</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="model" class="form-control" placeholder="Model" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="cc_reader_models">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'cc_reader_models'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 13 cc_antennas -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step cc_antennas']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Credit Card Antena</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="cc_antena" class="form-control" placeholder="Credit Card Antena" required></td>
                            
                            <td>
                                <input type="hidden" name="setting" value="cc_antennas">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'cc_antennas'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 14 bill_validator_models -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step bill_validator_models']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Model</th><th>Type</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="model" class="form-control" placeholder="Model" required></td>
                            <td>
                                <select name="type" class="form-control" required>
                                    <option value="">Please Select Type</option>
                                    <option value="MDB">MDB</option>
                                    <option value="Pulse">Pulse</option>
                                </select>
                            </td>
                            
                            <td>
                                <input type="hidden" name="setting" value="bill_validator_models">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'bill_validator_models'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 15 vault_models -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step vault_models']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Model</th><th>Lockable</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="model" class="form-control" placeholder="Model" required></td>
                            <td>
                                <select name="lockable" class="form-control" required>
                                    <option value="">Please Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </td>
                            
                            <td>
                                <input type="hidden" name="setting" value="vault_models">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'vault_models'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 16 vault_keys -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step vault_keys']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Vault Keys</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="vault_key" class="form-control" placeholder="Vault Key" required></td>                            
                            
                            <td>
                                <input type="hidden" name="setting" value="vault_keys">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'vault_keys'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}

            <!-- 17 hopper_models -->
            {!! Form::open(['url'=>'admin/machine_settings','class'=>'step hopper_models']) !!}
                
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr><th>SL</th><th>Company</th><th>Model</th><th>Size</th><th>Power</th><th></th></tr>
                    </thead>

                    <tbody>

                        <tr><td></td>                              
                            <td><input name="company" class="form-control" placeholder="Company" required></td>
                            <td><input name="model" class="form-control" placeholder="Model" required></td>
                            <td>
                                <select name="size" class="form-control" required>
                                    <option value="">Please Select</option>
                                    <option value="28mm">28mm</option>
                                    <option value="39mm">39mm</option>
                                </select>
                            </td>
                             <td>
                                <select name="power" class="form-control" required>
                                    <option value="">Please Select</option>
                                    <option value="24 V">24 V</option>
                                    <option value="110 V">110 V</option>
                                </select>
                            </td>
                            
                            <td>
                                <input type="hidden" name="setting" value="hopper_models">
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>
                            </td>
                        </tr>                

                        @include('admin.machine_settings.fetch',['setting'=>'hopper_models'])  
                        
                    </tbody>
                    
                </table>
            
            {!!Form::close() !!}
            

            
            
        </div> <!-- /.col-md-9 -->

        

    </div> <!-- /.row -->

    @include('admin.machine_settings.script')
    
@stop
