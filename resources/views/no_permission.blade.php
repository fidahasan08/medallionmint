<html>
<head>
	<title>No Permission Yet!</title>
</head>
<body>
	<h4>Sorry {{$user->name}}</h4>
	<p>You don't have permission to see any content. <a href="auth/logout">logout</a></p>
</body>
</html>