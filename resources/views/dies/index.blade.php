@extends('layout')

@section('main')
    <h1 class="page-header">Dies <small>({{$dies->count()}} item)</small></h1>

    <?php $sl=1;?>
    @foreach($dies as $item)
    	<div class="dies">
    		<div class="thumbnail">
    			{!! Html::image('upload/200-'.$item->photo, $item->photo, array('class' => '')) !!}

    		</div>
    		<h4 class="title">{{$sl++}}. {{$item->name}}</h4>
    	</div>
    @endforeach


@stop